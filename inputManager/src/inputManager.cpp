#include <inputManager.hpp>
#include <interpolation.hpp>
#include <iostream>
#include <fstream>
#include <instruction.hpp>
#include <host.hpp>
#include <telnetInput.hpp>

// TODO: rewrite all this

InputManager::InputManager(/* args */) {
  filepath_valid = 0;
}

InputManager::~InputManager() {
}

std::string InputManager::threadName() {
  return std::string("InputManager");
}

void InputManager::process() {

  /* Spawn the telnet thread */
  TelnetInput ti;
  ti.start();

  while(!stop_request) {

    while(!filepath_valid) {
      status = E_STATUS_IDLE;
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
      //logstring("Waiting for file input");
      continue;
    }

    /* Open Gcode file */
    std::ifstream file;
    file.open(filepath, std::ios::in);

    if (!file.is_open()) {
      logstring("Error opening file '" + filepath + "' !");
    }

    /* Loop for each line */
    std::string line;
    while(std::getline(file, line)) {

      /* Parse the line */
      Instruction* instruction;
      instruction = gcodeparser.parse(line);
      
      /* Queue the instruction if one was instanciated */
      /* Host thread will delete it once used */
      if (instruction != nullptr) {
        status_string = "Queued " + instruction->print();
        host.queueInstruction(instruction);
      }
      else {
        trace("Skipping line : " + line);
      }

    }
    
    filepath_valid = 0;

  }

}


void InputManager::setFile(std::string input_file_path) {

  filepath = input_file_path;
  filepath_valid = 1;

}



InputManager inputManager; 
