#ifndef INSTRUCTION_UTILS
#define INSTRUCTION_UTILS

#include <vector>
#include <map>

class instructionUtils {
  public:

  /* Finds each key/value pair from gcode */
  static std::map<std::string, double> parseArguments(std::string &gcode);

  /* Convert Gcode distances/lengths (mm or inches) to standard (meters) */
  static double gcodeLengthToStandard(double in);

  /* Convert Gcode speeds (mm/min or inches/min) to standard (m/s) */
  static double gcodeSpeedToStandard(double in);
};

#endif /* INSTRUCTION_UTILS */
