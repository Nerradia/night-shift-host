#ifndef INSTRUCTIONTEMP
#define INSTRUCTIONTEMP

#include <instruction.hpp>
#include <string>

class InstructionTemp : public Instruction {
  public:
  InstructionTemp(std::string gcode);
  ~InstructionTemp();

  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);
  
  void run();
  std::string print();

  private:
  int heater_id;
  bool is_blocking;
  double temp_target;

};

#endif /* INSTRUCTIONTEMP */
