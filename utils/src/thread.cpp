#include <thread.hpp>
#include <string.h>
#include <iostream>
#include <thread>
#include <pthread.h>
#include <config.hpp>

std::mutex* thread::coutmutex;
std::ofstream* thread::traceFile;

bool aThreadCrashed;

thread::thread() {
  stop_request = 0;

  /* On first thread instanciation, allocate mutex and file output */
  if (coutmutex == NULL) {
    coutmutex = new std::mutex();

    if (Config::traceAllToFile) {
      traceFile = new std::ofstream;
      traceFile->open("trace.log");
      *traceFile << "hey !" << std::endl;
    }
  }
}

thread::~thread() {
}

thread::E_status thread::getStatus() {
  enum E_status ret = status;
  return ret;
}

std::string thread::getStatusString() {
  // TODO: mutexes ?
  return status_string;
}

void thread::setStatus(enum E_status newStatus) {
  m_status.lock();
  status = newStatus;
  m_status.unlock();
}

void thread::setStatusString(std::string s) {
  m_status.lock();
  status_string = s;
  m_status.unlock();
}

void thread::start(/*bool realTimeThread*/) {
  thread_handle = new std::thread([this]() {
    try {
      this->process();
    }
    catch (const std::string &s) {
      aThreadCrashed = true;
      std::cout.flush();
      std::clog << std::endl << std::endl << "\033[1;41m                Thread " << this->threadName() << " died !                \033[0m" << std::endl;
      std::clog << "String : " << s << std::endl;
    }
    catch (const std::exception &e) {
      aThreadCrashed = true;
      std::cout.flush();
      std::clog << std::endl << std::endl << "\033[1;41m                Thread " << this->threadName() << " died !                \033[0m" << std::endl;
      std::clog << "what() : " << e.what() << std::endl;
    }
    catch (...) {
      aThreadCrashed = true;
      std::cout.flush();
      std::clog << std::endl << std::endl << "\033[1;41mThread " << this->threadName() << " died !\033[0m" << std::endl;
      std::exception_ptr p = std::current_exception();
      std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    std::cout << "\033[1;47m\033[30m                        Trace                        \033[0m" << std::endl;
    while(traceQueue.size()) {
      std::cout << traceQueue.front() << std::endl;
      traceQueue.pop();
    }
    std::cout.flush();
    abort();     
  });
 /* if (realTimeThread) {
    sched_param sch_params;
    sch_params.sched_priority = 2;
    pthread_setschedparam(thread_handle->native_handle(), SCHED_RR, &sch_params);
  }*/
}

void thread::stop() {
  stop_request = 1;
  thread_handle->join();
}

void thread::logstring(std::string s) {
  coutmutex->lock();
  std::cout << "[" << threadName() << "] " << s << std::endl;
  coutmutex->unlock();
}
 
void thread::trace(std::string s) {
  
  /* Write to file if enabled */
  if (Config::traceAllToFile) {
    coutmutex->lock();
    *traceFile << "[" << this->threadName() << "] " << s << std::endl;
    coutmutex->unlock();
  }

  /* Make room if needed */
  if (traceQueue.size() >= traceLogSize) {
    traceQueue.pop();
  }

  traceQueue.push(s);
}
