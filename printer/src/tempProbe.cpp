
#include <tempProbe.hpp>
#include <printer.hpp>
#include <cmath>
#include <limits>

TempProbe::TempProbe() {

}

TempProbe::~TempProbe() {

}


void TempProbe::configure(int adc_channel, double Rup, double Rdown, double r0, double t0, double B, std::string name) {

  //TODO: check if parameters are valid

  this->adc_channel = adc_channel;
  
  /* Thevenin stuff to simplify circuit */
  this->Rup_eq = (Rup * Rdown) / (Rup + Rdown);
  this->Vup_eq = 1 * Rdown / (Rup + Rdown);

  this->r0 = r0;
  this->t0 = t0 + 273.15;
  this->B = B;
  this->name = name;

}

std::string TempProbe::getName() {
  return name;
}

double TempProbe::getTemp() {

  // TODO: move ADC-specific stuff to a class like printer.adc or something
  int raw = printer.fpga.read(printer.fpga.adc_reader.adc_val[adc_channel]);
  /* Check ADC errors */
  int sig_bit = raw & (1 << 21);
  int msb_bit = raw & (1 << 20);

  if (raw == 0) {
    return std::numeric_limits<double>::quiet_NaN();
  }

  if (!sig_bit) {
    return std::numeric_limits<double>::quiet_NaN();
  }

  if (sig_bit && msb_bit) {
    return std::numeric_limits<double>::quiet_NaN();
  }
 
  /* Mask sig bit */
  raw &= 0x1FFFFF;

  double Vadc = Vup_eq * (double)raw / std::pow(2., (double)20);

  double Rctn = -(Vadc * Rup_eq) / (Vadc - Vup_eq);

  return -(273.15 * t0 * log(Rctn/r0) - (B * t0) + (273.15 * B)) / (B + t0 * log(Rctn/r0));

}
