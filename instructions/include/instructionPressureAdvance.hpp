#ifndef INSTRUCTIONPRESSUREADVANCE
#define INSTRUCTIONPRESSUREADVANCE

#include <instruction.hpp>
#include <string>

class instructionPressureAdvance : public Instruction {
  public:
  instructionPressureAdvance(std::string gcode);
  ~instructionPressureAdvance();
  
  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);

  void run();
  std::string print();

  private:
  double pressureAdvanceCoef;

};

#endif /* instructionPressureAdvance */
