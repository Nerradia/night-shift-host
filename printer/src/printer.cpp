#include <printer.hpp>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <cmath>

#include <config.hpp>

Printer::Printer() { 
  
  for (Driver &d : driver) {
    d.setSpi(&spi_periph);
  }

  blTouch.init(0, 0);

  path_out.open("printer_moves.csv");
}

Printer::~Printer() {
  path_out.close();
}

void Printer::init()  {
  std::cout << "Printer Init";
  fpga.init(dev_spi_fpga);
  spi_periph.init(dev_spi_periph, 3, 100000);
}

uint32_t Printer::getFifoWordCount() {
  return fpga.read(fpga.fifo_word_count);
}

double Printer::getFifoSize() {
  return (double)getFifoWordCount() / (double)FIFO_capacity_words;
}

std::string Printer::getFPGAVersion() {
  return fpga.read(fpga.Build_version);
}

std::string Printer::getFPGABuild() {
  return fpga.read(fpga.Build_date);
}

void Printer::setSyncValue(uint32_t val) {
  fpga.writeQueued(fpga.sync_register, val);
}

uint32_t Printer::getSyncValue() {
  return fpga.read(fpga.sync_register);
}

void Printer::setTempPriorityMask(int mask) {
  //TODO: check input
  fpga.write(fpga.adc_reader.mask_high_prio, mask);
  fpga.write(fpga.adc_reader.mask_low_prio, ~mask);
}


int Printer::getCurrentAdcChannel() {
  return fpga.read(fpga.adc_reader.current_channel);
}

void Printer::initDrivers() {

  spiSelectDriver(0);
  driver[0].init(0.9);
  spiSelectDriver(1);
  driver[1].init(0.9);
  spiSelectDriver(2);
  driver[2].init(0.6);
  spiSelectDriver(3);
  driver[3].init(0.9);
  spiSelectDriver(4);
  driver[4].init(0.9);
  spiSelectDriver(5);
  driver[5].init(0.9);
  spiSelectDriver(6);
  driver[6].init(0.9);
  spiSelectDriver(7);
  driver[7].init(0.9);
  spiSelectDriver(8);
  driver[8].init(0.9);
  spiSelectDriver(9);
  driver[9].init(0.9);
}

void Printer::enableDriver(int id) {
  uint32_t enable_mask = fpga.read(fpga.driver_controller.driver_enable);
  enable_mask |= (1 << id);
  fpga.write(fpga.driver_controller.driver_enable, enable_mask);
}

void Printer::disableDriver(int id) {
  uint32_t enable_mask = fpga.read(fpga.driver_controller.driver_enable);
  enable_mask &= ~(1 << id);
  fpga.write(fpga.driver_controller.driver_enable, enable_mask);
}

void Printer::spiLock() {
  spi_periph_m.lock();
}

void Printer::spiUnlock() {
  spi_periph_m.unlock();
}

void Printer::spiSelectDriver(int id) {
  uint32_t reg = 0;

  if (id < 0 || id > fpga.n_drivers) {
    throw std::out_of_range("Driver ID out of range");
  }

  reg |= 1 << 0;        /* drv_cs_enable */
  reg |= id << 1; /* drv cs mux */
  
  fpga.write(fpga.spi_mux.spi_mux, reg);
}

void Printer::spiSelectNone() {
  uint32_t reg = 0;

  fpga.write(fpga.spi_mux.spi_mux, reg);
}

void Printer::setOutputPower(int id, double power) {
  uint32_t reg;

  if (id < 0 || id > fpga.n_power_output - 1) {
    throw std::out_of_range("Power output ID out of range");
  }

  if (power < 0 || power > 1) {
    throw std::out_of_range("Power out of range");
  }

  unsigned char pwm;
  if (power < 0.01) {
    pwm = 0;
  } else 
  if (power > 0.99) {
    pwm = 255;
  } else {
    pwm = (unsigned char) (power * 256.);
  }
  
  reg = fpga.read(fpga.power_output.power_output[id]);

  reg &= ~(0xFF << 0); /* Clear PWM field */
  reg |= pwm << 0;     /* Write new PWM value */

  fpga.write(fpga.power_output.power_output[id], reg);
}

void Printer::setOutputFreq(int id, double freq) {
  uint32_t reg;
  const double freq_max = 1000000. / 256.;
  const double freq_min = 1000000. / 65535. / 256.;

  if (id < 0 || id > fpga.n_power_output - 1) {
    throw std::out_of_range("Power output ID out of range");
  }

  if (freq < freq_min || freq > freq_max) {
    throw std::out_of_range("Frequency out of range");
  }

  uint16_t clk_div;
  
  clk_div = (uint16_t) (1000000. / (freq * 256.));
  
  reg = fpga.read(fpga.power_output.power_output[id]);

  reg &= ~(0xFFFF << 8); /* Clear clock divider field */
  reg |= clk_div << 8;     /* Write new value */

  fpga.write(fpga.power_output.power_output[id], reg);
}

void Printer::setOutput(int id, double power, double freq) {
  setOutputFreq(id, freq);
  setOutputPower(id, power);
}

void Printer::setOutputWatchdogMask(int32_t mask) {

  fpga.write(fpga.power_output.watchdog_mask, mask);
}

int32_t Printer::getCurrentPos(int driverId) {
  
  if (driverId < 0 || driverId > fpga.n_drivers - 1) {
    throw std::out_of_range("Driver ID out of range");
  }

  uint32_t reg;
  reg = fpga.read(fpga.position_controller.current_pos[driverId]);
  
  return (int32_t) reg;
}

void Printer::resetCurrentPos() {
  /* Data written is insignificant, only the writing action matters */
  fpga.write(fpga.position_controller.position_reset, 0);
}

int32_t Printer::getCapturedPos(int driverId) {  

  if (driverId < 0 || driverId > fpga.n_drivers - 1) {
    throw std::out_of_range("Driver ID out of range");
  }

  uint32_t reg;
  reg = fpga.read(fpga.position_controller.captured_pos[driverId]);
  
  return (int32_t) reg;
}

void Printer::setTriggerInput(int triggerId, int inputId) {

  if (triggerId < 0 || triggerId >= fpga.n_triggers) {
    throw std::out_of_range("Trigger ID out of range");
  }

  if (inputId < 0 || inputId >= fpga.n_trigger_inputs) {
    throw std::out_of_range("Input ID out of range");
  }

  fpga.write(fpga.position_controller.trigger_in_sel[triggerId], inputId);

}

void Printer::setMotorTrigger(int driverId, int triggerId) {

  if (driverId < 0 || driverId >= fpga.n_drivers) {
    throw std::out_of_range("Driver ID out of range");
  }

  if (triggerId < 0 || triggerId >= fpga.n_triggers) {
    throw std::out_of_range("Input ID out of range");
  }

  fpga.write(fpga.position_controller.drv_sel[driverId], triggerId);

}

void Printer::setMotorActions(int driverId, bool capture, bool zero, bool inhibit) {

  if (driverId < 0 || driverId >= fpga.n_drivers) {
    throw std::out_of_range("Driver ID out of range");
  }

  int reg_val = 0;

  if (zero) {
    reg_val |= 1 << 2;
  }

  if (capture) {
    reg_val |= 1 << 1;
  }

  if (inhibit) {
    reg_val |= 1 << 0;
  }

  fpga.write(fpga.position_controller.drv_action[driverId], reg_val);
}

TriggerMask Printer::getTriggerState() {
  
  int reg_val = fpga.read(fpga.position_controller.trigger_state);
  
  return TriggerMask(reg_val);
}

bool Printer::getTriggerState(int triggerId) {

  if (triggerId < 0 || triggerId >= fpga.n_triggers) {
    throw std::out_of_range("Trigger ID out of range");
  }

  return getTriggerState().getBit(triggerId);
}

void Printer::resetTrigger(int triggerId) {
  
  if (triggerId < 0 || triggerId >= fpga.n_triggers) {
    throw std::out_of_range("Trigger ID out of range");
  }

  fpga.write(fpga.position_controller.trigger_reset, 1 << triggerId);
}

void Printer::resetTriggers() {
  
  fpga.write(fpga.position_controller.trigger_reset, ~0);
}

void Printer::setEndstopPolarity(TriggerMask mask) {
  
  fpga.write(fpga.position_controller.polarity_swap, mask.getIntMask());
}

bool Printer::motionIsIdle() {
  return getFifoSize() == 0;
}

void Printer::setServoAngle(int id, double deg) {

  /* 0 to 180° -> 1 ms to 2 ms */
  int val = 1000 + 1000. * deg / 180.;

  fpga.write(fpga.servo_controller.servo[id], val);
}


void Printer::setServoTime(int id, double us) {

  fpga.write(fpga.servo_controller.servo[id], (int)us);
}


void Printer::queueMove(PrinterMove move) {

 // if (!Config::simu) {  
 //   path_out << "\r\n-> queueMove Called";
 //   currentWC = getFifoWordCount();
//
 //   path_out << "getFifoWordCount : " << getFifoWordCount() << std::endl;
 // }
 
  /* Calculate step generator's clock divider for this speed */
  uint32_t clock_div = double(FPGA::clk_frequency) / move.maxStepFreq;
 
  //std::cout << "Clkdiv : " << clock_div << " Max : " << longest_path << " target[1]=" << move.target[1] << std::endl;

  int32_t maxSteps = std::abs(move.target[0]);
  for (int i = 0; i < Config::n_motors; i++) {
    maxSteps = std::max(maxSteps, std::abs(move.target[i]));
  }
  
  double travelTime = maxSteps / move.maxStepFreq;

  /* Log to file */
  for (int i = 0; i < FPGA::n_drivers; i++) {
    if (i) {
      path_out << ", ";
    }   
    path_out << move.target[i];
  }
  path_out << ", " << clock_div << std::endl;

  if (Config::simu) {  
    std::this_thread::sleep_for(std::chrono::microseconds((int) (travelTime * 1000000)));
  }
  else {
    /* Queue in FPGA */
   // path_out << "Queuing to FPGA\r\n";
    int count = 0;
    while ((count = getFifoWordCount()) > 8150) {
      std::this_thread::yield();
      //std::this_thread::sleep_for(std::chrono::microseconds(10));
    }
    
    fpga.moveQueued(clock_div, maxSteps, move.target, path_out);
    
  //  path_out << "Waiting : ";
   //   path_out << count << " ";
   // path_out << "Done !\r\n";
  }
}


TriggerMask::TriggerMask() {
  mask = 0;
}

TriggerMask::TriggerMask(uint32_t intMask) {
  mask = intMask;
}

void TriggerMask::setBit(int id) {
  if (id < 0 || id > 31) {
    throw std::out_of_range("Mask bit ID out of range");
  }

  mask |= (1 << id);
}

void TriggerMask::setBit(int id, int val) {
  if (id < 0 || id > 31) {
    throw std::out_of_range("Mask bit ID out of range");
  }

  if (val) {
    mask |= (1 << id);
  } else {
    mask &= ~(1 << id);
  }
}

bool TriggerMask::getBit(int id) {
  if (id < 0 || id > 31) {
    throw std::out_of_range("Mask bit ID out of range");
  }
  return (mask & (1 << id));
}

void TriggerMask::resetBit(int id) {
  if (id < 0 || id > 31) {
    throw std::out_of_range("Mask bit ID out of range");
  }

  mask &= ~(1 << id);
}

void TriggerMask::clear() {
  mask = 0;
}

void TriggerMask::setAll() {
  mask = ~0;
}

uint32_t TriggerMask::getIntMask() {
  return mask;
}

PrinterMove::PrinterMove() {
  for (int i = 0; i < FPGA::n_drivers; i++) {
    target[i] = 0;
  }
}

PrinterMove::~PrinterMove() {
}


Printer printer;
