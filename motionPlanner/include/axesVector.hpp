#ifndef AXESVECTOR
#define AXESVECTOR

#include <config.hpp>

/* A vector with the same number of dimensions as Config::n_axes */
class AxesVector : public std::array<double, Config::n_axes> {
  public:
  AxesVector(std::array<double, Config::n_axes> &v);
  AxesVector();

  /* Length of the vector */
  double norm();

  /* Sum of all elements */
  double sum();

  /* Debug print */
  std::string print();

  
  AxesVector operator=(std::array<double, Config::n_axes> &in);
  AxesVector operator+(AxesVector &a);
  AxesVector operator-(AxesVector &a);
  AxesVector operator*(AxesVector &a);
  AxesVector operator*(double a);
  AxesVector operator/(double a);
};


AxesVector operator+(const AxesVector &a, const AxesVector &b);
AxesVector operator-(const AxesVector &a, const AxesVector &b);
AxesVector operator/(const AxesVector &a, const AxesVector &b);

double max(const AxesVector &a);
AxesVector abs(const AxesVector &a);

#endif /* AXESVECTOR */
