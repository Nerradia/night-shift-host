#include <motionPlanner.hpp>
#include <thread.hpp>

MotionPlanner motionPlanner;


MotionPlanner::MotionPlanner() {
  motionOutput = new MotionOutput(&outputQueue);
  interpolation = new Interpolation(&inputQueue, &accelQueue);
  accelerationPlanner = new AccelerationPlanner(&accelQueue, &outputQueue);
  currentPosition.fill(0);
  hostOffset.fill(0);
}

MotionPlanner::~MotionPlanner() {

}

void MotionPlanner::start() {
  interpolation->start();
  accelerationPlanner->start();
  motionOutput->start();
}

void MotionPlanner::stop() {

}

void MotionPlanner::flush() {
  /* Send flush requests everywhere */
  interpolation->flush();
  accelerationPlanner->flush();
  motionOutput->flush();

  /* Wait until every thread flushed its input */
  waitIDLE();
}

void MotionPlanner::resync() {

  /* Get the FPGA's position */
  motionOutput->resync();

  /* Update motionplanner's position accordingly */
  currentPosition = motionOutput->getPosition();
}

void MotionPlanner::waitIDLE() {
  bool idle;
  do {
    idle = (interpolation->getStatus() == thread::E_STATUS_IDLE) 
        && (accelerationPlanner->getStatus() == thread::E_STATUS_IDLE) 
        && (motionOutput->getStatus() == thread::E_STATUS_IDLE)
        && printer.motionIsIdle()
        && allQueuesEmpty();
    if (!idle) {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
  } while (!idle);
}


void MotionPlanner::setLinearAdvance(double factor) {
  motionOutput->setLinearAdvance(factor);
}

void MotionPlanner::queueMove(AxesVector target, double speed) {

  Move m;
  m.origin = currentPosition;
  m.target = target + hostOffset;
  m.targetSpeed = speed;
  
  /* Don't register tiny movements */
  if (m.length() < 0.0001) return;

  /* Limit speed */
  double speed_factor = 1;
  for (int i = 0; i < Config::n_axes; i++) {
    speed_factor = std::min(speed_factor, Config::axisMaxSpeed[i] / m.getTargetSpeed(i));
  }
  m.targetSpeed *= speed_factor;

  /* Wait until there is room */
  while (inputQueue.size() > Config::motionPlannerInputQueueSize) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  /* Queue */
  inputQueue.push(m);

  currentPosition = m.target;
}

void MotionPlanner::setCurrentPosition(AxesVector hostPosition) {
  hostOffset = currentPosition - hostPosition;
}

double MotionPlanner::getHostOffset(int axis) {
  return hostOffset[axis];
}


double MotionPlanner::getCapturedPosition(int axis) {

  int stepperId = Config::axisToStepper[axis].at(0);
  auto steps = printer.getCapturedPos(stepperId);
  return (double) steps * Config::stepperResolution[stepperId] - hostOffset[axis];
}

void MotionPlanner::setStepperOffset(int stepperId, double offset) {
  motionOutput->setStepperOffset(stepperId, offset);
}

void MotionPlanner::setBedLevelingOffsets(std::vector<std::vector<double>> map, int np, double tileSizeX, double tileSizeY, double offsetX, double offsetY) {
  motionOutput->setBedLevelingOffsets(map, np, tileSizeX, tileSizeY, offsetX, offsetY);
}


double MotionPlanner::getQueueSize(int step) {
  switch(step) {
    case 0: 
      return (double) inputQueue.size() / Config::motionPlannerInputQueueSize;

    case 1: 
      return (double) accelQueue.size() / Config::motionPlannerAccelQueueSize;

    case 2: 
      return (double) outputQueue.size() / Config::motionPlanneroutputQueueSize;

    default:
      return 0;
  }
}

double MotionPlanner::getMPCurrentPosition(int axis) {
  return currentPosition[axis];
}

AxesVector MotionPlanner::getMPCurrentPosition() {
  return currentPosition;
}

bool MotionPlanner::allQueuesEmpty() {
  return (inputQueue.size() == 0) && (accelQueue.size() == 0) && (outputQueue.size() == 0);
}
