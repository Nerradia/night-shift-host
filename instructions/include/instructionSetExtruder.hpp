#ifndef INSTRUCTIONSETEXTRUDER
#define INSTRUCTIONSETEXTRUDER

#include <instruction.hpp>
#include <string>

class InstructionSetExtruder : public Instruction {
  public:
  InstructionSetExtruder(std::string gcode);
  ~InstructionSetExtruder();
  
  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);

  void run();
  std::string print();

  private:
  int extruderId;
  bool onlySwitch;
};

#endif /* INSTRUCTIONSETEXTRUDER */
