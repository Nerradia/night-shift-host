
#include <bltouch.hpp>
#include <printer.hpp>

BLTouch::BLTouch() {

}

BLTouch::~BLTouch() {

}

void BLTouch::init(int servoId, int endstopId) {
  this->servoId = servoId;
  this->endstopId = endstopId;
}

void BLTouch::reset() {
  printer.setServoTime(servoId, 2190);
}

void BLTouch::probe() {
  printer.setServoTime(servoId, 650);
}

void BLTouch::retract() {
  printer.setServoTime(servoId, 1475);
}

void BLTouch::selfTest() {
  printer.setServoTime(servoId, 1780);
}

int BLTouch::getEndstopId() {
  return endstopId;
}


