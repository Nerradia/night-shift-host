
#include <driver.hpp>
#include <cmath>

Driver::Driver() {
  spi = nullptr;

}

Driver::~Driver() {

}

void Driver::setSpi(Spi* in) {
  spi = in;
}

// Temporary stuff
void Driver::sendTMC(char reg, uint32_t data) {
	char buffer_tx[5] = {
			(char) (reg | 0x80),
			(char) (data >> 24),
			(char) (data >> 16),
			(char) (data >> 8),
			(char) data
	};
  
  char buffer_rx[5];
  
  spi->readWrite(buffer_tx, buffer_rx, 5);
  spi->readWrite(buffer_tx, buffer_rx, 5);

  //printf("Rx : %02x %02x %02x %02x %02x\r\n", buffer_rx[0], buffer_rx[1], buffer_rx[2], buffer_rx[3], buffer_rx[4]);

}


void Driver::init(double RMSAmps) {
  if (spi == nullptr) {
    throw std::string("Driver init() called but no SPI pointer set");
  }

  /* Calculate the current scale */
  uint8_t CS = std::ceil(RMSAmps / 1.64 * 32);

  sendTMC(0xEC, 0x200100C3);

  sendTMC(0x90, 0x00060000 | (CS << 8) | (CS/2 << 0));
  
  sendTMC(0x91, 0x0000000A);
  sendTMC(0x80, 0x00000000);
  sendTMC(0x93, 0x000001F4);// * 100);
  sendTMC(0xF0, 0x000401C8);

}

