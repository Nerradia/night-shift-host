#include <gcodeParser.hpp>

#include <instruction.hpp>
#include <instructionMove.hpp>
#include <instructionHome.hpp>
#include <instructionWait.hpp>
#include <instructionTemp.hpp>
#include <instructionSetPosition.hpp>
#include <instructionPressureAdvance.hpp>
#include <instructionBedLeveling.hpp>
#include <instructionSetExtruder.hpp>
#include <instructionFan.hpp>

gcodeParser::gcodeParser() {

  std::vector<instructionDefinition> instructions;

  /* Make a list of every instruction implemented */
  instructions.push_back(InstructionMove::getInstructionDefinition());
  instructions.push_back(InstructionSetPosition::getInstructionDefinition());
  instructions.push_back(instructionPressureAdvance::getInstructionDefinition());

  /* Some instructions are not simulated */
  if (!Config::simu) {
    instructions.push_back(InstructionHome::getInstructionDefinition());
    instructions.push_back(InstructionWait::getInstructionDefinition());
    instructions.push_back(InstructionTemp::getInstructionDefinition());
    instructions.push_back(InstructionFan::getInstructionDefinition());
    instructions.push_back(InstructionBedLeveling::getInstructionDefinition());
    instructions.push_back(InstructionSetExtruder::getInstructionDefinition());
  }

  /* Map gcode instructions to the constructor */
  for(const auto& inst : instructions) {
    for(const auto& gcodeTok : inst.gcodeTokens) {

      /* Check if it is not already defined */
      if (instructionMap.find(gcodeTok) == instructionMap.end()) {
        instructionMap[gcodeTok] = inst.constructor;
      } 
      else {
        throw std::string("Instruction " + gcodeTok + " has been mapped twice !");
      }
      
    }
  }



}

gcodeParser::~gcodeParser() {
}

Instruction* gcodeParser::parse(std::string gcode) {
  Instruction* instruction = nullptr;
      
  char gcode_cs[gcode.length() + 1];
  strcpy(gcode_cs, gcode.data());

  char command[16];

  /* TODO: rewrite this mess */
  int i = 0;
  while(gcode_cs[i]) {
    char c = gcode_cs[i];

    /* End parsing if it is a comment */
    if (c == ';') {
      break;
    }

    /* Beginning of a command */
    if (isalpha(c)) {
      strcpy(command, &gcode_cs[i]);
      
      /* Cut when a space is found */
      int j = 0;
      while(command[j]) {
        if (command[j] == ' ' || command[j] == '\r' || command[j] == '\n' ) {
          command[j] = '\0';
        }
        j++;
      }

      /* Find if there is an instruction for this */
      if (instructionMap.find(command) != instructionMap.end()) {
        /* Call the mapped constructor */
        instruction = instructionMap[command](gcode);
      } else {
        /* TODO: do something if no instruction found */
      }

      break;
    }
    i++;
  }

  return instruction;
}

