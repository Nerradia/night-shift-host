
#include <instructionHome.hpp>
#include <host.hpp>

/* Mapping functions */
instructionDefinition InstructionHome::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionHome::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionHome::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("G28");

  return list;
}

Instruction* InstructionHome::mapHook(std::string gcode) {
  return new InstructionHome(gcode);
}


InstructionHome::InstructionHome(std::string gcode) {

  auto gcodeData = instructionUtils::parseArguments(gcode);

  homing_mask.fill(false);

  bool atLeastOneDefined = false;
  /* Check what axis is mentionned for homing */
  for (int i = 0; i < Config::n_axes; i++) {
    std::string axisName = Config::axisToGcodeMap.at(i);

    /* Axis i is on the list, enable its homing */
    if (gcodeData.find(axisName) != gcodeData.end()) {
      atLeastOneDefined = true;
      homing_mask[i] = true;
    }
  }

  /* No axis given, that means every axis */
  if (atLeastOneDefined == false) { 
    homing_mask.fill(true);
  }

}

InstructionHome::~InstructionHome() {

}

std::string InstructionHome::print() {
  std::string s;
  s += "Home axes ";
  for(int i = 0; i < Config::n_axes; i++) {
    if (homing_mask[i]) {
      s += std::to_string(i) + " ";
    }
  }
  return s;
}

/* TODO: make the sequence configurable */
void InstructionHome::run() {

  /* Block until everything is done */
  host.waitMotionPipelineEmpty();

  /* Go up a bit to be safe */
  host.setSpeed(Config::homing_speed_XY);
  host.setTargetRelative(2, 0.020);
  host.goToTarget();

  /* Block until everything is done */
  host.waitMotionPipelineEmpty();

  /* Setup all homing axes's triggers and request a move to homing direction */
  for(auto const axis : Config::homing_axes) {
    if (homing_mask[axis]) {
      host.setEndstop(axis);
      host.resetEndstopState(axis);
      host.setTargetRelative(axis, Config::homing_direction[axis]);
    }
  }
  
  host.setSpeed(Config::homing_speed_XY);
  
  /* Request a move to the origin */
  host.goToTarget();

  bool done = 0;
  do {
    done = true;
    
    for(auto const axis : Config::homing_axes) {
      if (homing_mask[axis]) {
        done &= host.getEndstopState(axis);
      }
    }

    /* Don't hog the SPI, wait a bit */
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

  } while (!done);

  /* Both axes hit home, stop queuing moves */
  host.flushMotionPipeline();

  /* Wait until the FPGA FIFO is empty */
  host.waitMotionPipelineEmpty();
  
  /* Motion pipeline was flushed, reset it and tell where we are now */
  host.syncMotionPipeline();

  /* Update host position */
  for(auto const axis : Config::homing_axes) {
    if (homing_mask[axis]) {
      host.setCurrentPosition(axis, Config::homing_position[axis]);
    }
  }
  
  /* Clear endstops to allow the printer to move again */
  for(auto const axis : Config::homing_axes) {
    if (homing_mask[axis]) {
      host.freeEndstop(axis);
    }
  }
  
  /* Probe Z */
  if(homing_mask[Config::probing_axis]) {

    /* Move to probing point */
    host.setTargetAbsolute(0, Config::probing_point[0]);
    host.setTargetAbsolute(1, Config::probing_point[1]);
    host.setSpeed(Config::homing_speed_XY);
    host.goToTarget();

    /* Wait until the move is done */
    host.waitMotionPipelineEmpty();

    /* Prepare the BLtouch */
    host.armBLtouch();

    /* Set BLTouch's output action */
    host.setEndstop(2);

    /* Go down */
    host.setSpeed(Config::homing_speed_Z);
    host.setTargetRelative(2, Config::homing_direction[2]);
    host.goToTarget();
    
    /* Wait until endstop is hit */
    while(!host.getEndstopState(2)) {
      /* Don't hog the SPI */
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    
    /* axis hit home, stop queuing moves */
    host.flushMotionPipeline();

    /* Wait until the FPGA FIFO is empty */
    host.waitMotionPipelineEmpty();

    /* Retract the BLTouch */
    host.disarmBLtouch();
    
    /* Motion pipeline was flushed, reset it */
    host.syncMotionPipeline();

    /* Update position */
    host.setCurrentPosition(Config::probing_axis, Config::probing_point[Config::probing_axis]);
    
    /* Allow motor to move */
    host.resetEndstopState(2);

    /* Disable the endstop action */
    host.freeEndstop(2);

    /* Move up a bit */
    host.setSpeed(Config::homing_speed_XY);
    host.setTargetAbsolute(2, 0.020);
    host.goToTarget();
  }
  
  /* Block until everything is done */
  host.waitMotionPipelineEmpty();
}
