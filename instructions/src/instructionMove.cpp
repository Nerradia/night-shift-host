
#include <instructionMove.hpp>

#include <interpolation.hpp>
#include <host.hpp>

/* Mapping functions */
instructionDefinition InstructionMove::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionMove::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionMove::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("G0");
  list.push_back("G1");

  return list;
}

Instruction* InstructionMove::mapHook(std::string gcode) {
  return new InstructionMove(gcode);
}

InstructionMove::InstructionMove(std::string gcode) {
  
  /* Parse input */
  gcodeData = instructionUtils::parseArguments(gcode);

}

InstructionMove::~InstructionMove() {

}

std::string InstructionMove::print() {
  std::string s;
  s += "Move ";
  if (gcodeData["G"] == 0.) {
    s += "(G0) ";
  } else {
    s += "(G1) ";
  }
  for(const auto arg : gcodeData) {
    if (arg.first != "G") {
      s += arg.first + ":" + std::to_string(arg.second) + " ";
    }
  }
  return s;
}

void InstructionMove::run() {

  /* Apply requested changes */
  for (int i = 0; i < Config::n_axes; i++) {
    std::string axisName = Config::axisToGcodeMap.at(i);

    /* Gcode doesn't define what doesn't change, so check if current 
       axis is mentionned */
    if (gcodeData.find(axisName) != gcodeData.end()) {
      double val = instructionUtils::gcodeLengthToStandard(gcodeData[axisName]);
      
      if (host.pos.mode == host.POS_ABSOLUTE) {
        /* Absolute mode */
        host.setTargetAbsolute(i, val);
      } 
      else { 
        /* Relative mode */
        host.setTargetRelative(i, val);
      }

    }
  }

  /* Set speed if defined */
  if (gcodeData.find("F") != gcodeData.end()) {
    double val = instructionUtils::gcodeSpeedToStandard(gcodeData["F"]);
    host.setSpeed(val);
  }

  host.goToTarget();

}
