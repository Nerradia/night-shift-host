#ifndef INSTRUCTIONSETPOSITION
#define INSTRUCTIONSETPOSITION

#include <instruction.hpp>
#include <string>

class InstructionSetPosition : public Instruction {
  public:
  InstructionSetPosition(std::string gcode);
  ~InstructionSetPosition();
  
  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);

  void run();
  std::string print();

  private:
  struct axisToPosition {
    int axis;
    double position;
  };

  std::vector<struct axisToPosition> data;

};

#endif /* INSTRUCTIONSETPOSITION */
