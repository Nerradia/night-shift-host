#ifndef PRINTER_HPP
#define PRINTER_HPP

#include <fpga.hpp>
#include <driver.hpp>
#include <tempProbe.hpp>
#include <bltouch.hpp>
#include <fstream>
#include <mutex>


class PrinterMove {
  public:
    PrinterMove();
    ~PrinterMove();
    /* target number of steps to do */
    int32_t target[FPGA::n_drivers];
    /* step generator frequency. Must be the frequency of the fastest axis */
    double maxStepFreq;
    int moveID;
};

class TriggerMask {
  public:
  TriggerMask();
  TriggerMask(uint32_t intMask);
  void setBit(int id);
  void setBit(int id, int val);
  bool getBit(int id);
  void resetBit(int id);
  void clear();
  void setAll();

  uint32_t getIntMask();
  private:
  uint32_t mask;
};

class Printer {
  public:
  Printer();
  ~Printer();

  void init();

  std::string getFPGAVersion();
  std::string getFPGABuild();

  void setSyncValue(uint32_t val);
  uint32_t getSyncValue();

  uint32_t getFifoWordCount();
  double getFifoSize();
  bool motionIsIdle();

  void initDrivers();
  void enableDriver(int id);
  void disableDriver(int id);

  /* Peripheral SPI has to be locked before use */
  void spiLock();
  void spiUnlock();
  void spiSelectDriver(int id);
  void spiSelectNone();

  void setTempPriorityMask(int mask);
  int getCurrentAdcChannel();
  
  void setOutputPower(int id, double power);
  void setOutputFreq(int id, double freq);
  void setOutput(int id, double power, double freq);
  void setOutputWatchdogMask(int32_t mask);

  /* Resets all current positions registers */
  void resetCurrentPos();
  
  int32_t getCurrentPos(int driverId);
  int32_t getCapturedPos(int driverId);
  void setTriggerInput(int triggerId, int inputId);
  void setMotorTrigger(int driverId, int triggerId);
  void setMotorActions(int driverId, bool capture, bool zero, bool inhibit);
  TriggerMask getTriggerState();
  bool getTriggerState(int triggerId);
  void resetTrigger(int triggerId);
  void resetTriggers();
  void setEndstopPolarity(TriggerMask mask);

  void queueMove(PrinterMove move);
  
  void setServoAngle(int id, double deg);
  void setServoTime(int id, double us);
  
  // TODO : This is a bit redundant with fpga's constant
  static const int n_motors = 10; 
  static constexpr int FIFO_capacity_words = 4096; 

  // TODO : put private
  //private:
  FPGA fpga;

  const char* dev_spi_fpga = "/dev/spidev0.0";
  const char* dev_spi_periph = "/dev/spidev0.1";

  int currentWC;
  
  Spi spi_periph;
  std::mutex spi_periph_m;
  TempProbe tempProbe[7];
  BLTouch blTouch;

  Driver driver[n_motors];

  std::ofstream path_out;

};

extern Printer printer;

#endif //PRINTER_HPP
