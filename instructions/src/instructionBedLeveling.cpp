
#include <instructionBedLeveling.hpp>
#include <host.hpp>

/* Mapping functions */
instructionDefinition InstructionBedLeveling::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionBedLeveling::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionBedLeveling::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("G29");

  return list;
}

Instruction* InstructionBedLeveling::mapHook(std::string gcode) {
  return new InstructionBedLeveling(gcode);
}


InstructionBedLeveling::InstructionBedLeveling(std::string gcode) {
  /* No arguments yet */
}

InstructionBedLeveling::~InstructionBedLeveling() {

}

std::string InstructionBedLeveling::print() {
  std::string s;
  s += "Level bed ";
  return s;
}


void InstructionBedLeveling::probePoint(class ProbePoint &p) {

  /* Go to safe height */
  host.setSpeed(Config::bedleveling_speed_travel);
  host.setTargetAbsolute(2, Config::bedleveling_safe_height);
  host.goToTarget();
  
  /* Go to a point */
  host.setTargetAbsolute(0, p.X);
  host.setTargetAbsolute(1, p.Y);
  host.goToTarget();
  host.waitMotionPipelineEmpty();

  /* Prepare the BLtouch */
  host.armBLtouch();

  /* Set BLTouch's output action */
  host.setProbe(2);

  host.resetEndstopState(2);

  /* Go down fast to probing height then slow */
  host.setSpeed(Config::bedleveling_speed_travel);
  host.setTargetAbsolute(2, Config::bedleveling_probing_height);
  host.goToTarget();
  
  host.setSpeed(Config::bedleveling_speed_probing);
  host.setTargetAbsolute(2, Config::bedleveling_probing_target);
  host.goToTarget();

  /* Wait until endstop is hit */
  while(!host.getEndstopState(2)) {
    /* Don't hog the SPI */
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  
  /* axis hit home, stop queuing moves */
  host.flushMotionPipeline();

  /* Wait until the FPGA FIFO is empty */
  host.waitMotionPipelineEmpty();

  /* Retract the BLTouch */
  host.disarmBLtouch();
  
  /* Motion pipeline was flushed, reset it */
  host.syncMotionPipeline();

  double measured = host.getCapturedPosition(2);

  /* Update our Z position to where the trigger happened */
  host.setCurrentPosition(2, measured);

  p.Z = measured - Config::bltouch_offset;
    
  /* Allow motor to move */
  host.resetEndstopState(2);

  /* Disable the endstop action */
  host.freeEndstop(2);

  /* Go to safe height */
  host.setSpeed(Config::bedleveling_speed_travel);
  host.setTargetAbsolute(2, Config::bedleveling_safe_height);
  host.goToTarget();
  host.waitMotionPipelineEmpty();
}

std::vector<InstructionBedLeveling::ProbePoint> InstructionBedLeveling::probeArea(int nPoints) {
  /* Block until everything is done */
  host.waitMotionPipelineEmpty();

  /* Determine left edge of the probing area */
  double probingAreaMinX;
  if (Config::bedleveling_probe_offset_x > 0) {
    probingAreaMinX = 0;
  }
  else {
    probingAreaMinX = -Config::bedleveling_probe_offset_x;
  } 

  /* bottom edge */
  double probingAreaMinY;
  if (Config::bedleveling_probe_offset_y > 0) {
    probingAreaMinY = 0;
  }
  else {
    probingAreaMinY = -Config::bedleveling_probe_offset_y;
  } 

  /* Right edge */
  double probingAreaMaxX;
  if (Config::bedleveling_probe_offset_x > 0) {
    probingAreaMaxX = Config::printersize_x;
  }
  else {
    probingAreaMaxX = Config::printersize_x + Config::bedleveling_probe_offset_x;
  } 

  /* Top edge */
  double probingAreaMaxY;
  if (Config::bedleveling_probe_offset_y > 0) {
    probingAreaMaxY = Config::printersize_y;
  }
  else {
    probingAreaMaxY = Config::printersize_y + Config::bedleveling_probe_offset_y;
  } 

  double probingAreaXlen = probingAreaMaxX - probingAreaMinX;
  double probingAreaYlen = probingAreaMaxY - probingAreaMinY;
  
  std::vector<ProbePoint> pointList;

  for (int ix = 0; ix < nPoints; ix++) {
    for (int iy = 0; iy < nPoints; iy++) {
      pointList.push_back({probingAreaMinX + probingAreaXlen * ix / (nPoints-1), probingAreaMinY + probingAreaYlen * iy / (nPoints-1), 0});
    }
  }

  for (auto &p : pointList) {
    probePoint(p);
  }

  for (auto p : pointList) {
    host.traceStr("Point : " + std::to_string(p.X) + ", " + std::to_string(p.Y) + ", " + std::to_string(p.Z));
  }
  
  return pointList;
}

void InstructionBedLeveling::run() {

  /* First correct the corners by moving each Z stepper independently */
  /* Measure Z at each corner (2x2 matrix) */
  auto corners = probeArea(2);

  /* Apply offsets to all but the first stepper (our reference) */
  host.traceStr("Offset matrix for Z-axis steppers : "
                  + std::to_string(- corners.at(3).Z + corners.at(3).Z) + ", "
                  + std::to_string(- corners.at(2).Z + corners.at(3).Z) + ", "
                  + std::to_string(- corners.at(1).Z + corners.at(3).Z) + ", "
                  + std::to_string(- corners.at(0).Z + corners.at(3).Z) + ", ");

  host.setStepperOffset(6, (- corners.at(2).Z + corners.at(3).Z) * 1.1);
  host.setStepperOffset(8, (- corners.at(1).Z + corners.at(3).Z) * 1.1);
  host.setStepperOffset(9, (- corners.at(0).Z + corners.at(3).Z) * 1.1);

  /* Now measure Z in a 5x5 matrix */
  auto bedLevel = probeArea(Config::bedleveling_n_points);

  /* Re-organize that 
     TODO: change probeArea to output the good stuff directly or something */

  /*std::vector<std::vector<double>> map = {
    {bedLevel[ 0].Z, bedLevel[ 1].Z, bedLevel[ 2].Z, bedLevel[ 3].Z, bedLevel[ 4].Z},
    {bedLevel[ 5].Z, bedLevel[ 6].Z, bedLevel[ 7].Z, bedLevel[ 8].Z, bedLevel[ 9].Z},
    {bedLevel[10].Z, bedLevel[11].Z, bedLevel[12].Z, bedLevel[13].Z, bedLevel[14].Z},
    {bedLevel[15].Z, bedLevel[16].Z, bedLevel[17].Z, bedLevel[18].Z, bedLevel[19].Z},
    {bedLevel[20].Z, bedLevel[21].Z, bedLevel[22].Z, bedLevel[23].Z, bedLevel[24].Z},
    };*/

  std::vector<std::vector<double>> map = {
  {bedLevel[0].Z, bedLevel[1].Z, bedLevel[2].Z},
  {bedLevel[3].Z, bedLevel[4].Z, bedLevel[5].Z},
  {bedLevel[6].Z, bedLevel[7].Z, bedLevel[8].Z}
  };

  host.setBedLevelingOffsets(map, Config::bedleveling_n_points, Config::printersize_x/Config::bedleveling_n_points, Config::printersize_y/Config::bedleveling_n_points, 0, 0);

  host.armBLtouch();
  host.disarmBLtouch();

}
