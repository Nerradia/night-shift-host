#ifndef INSTRUCTIONFAN
#define INSTRUCTIONFAN

#include <instruction.hpp>
#include <string>

class InstructionFan : public Instruction {
  public:
  InstructionFan(std::string gcode);
  ~InstructionFan();

  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);
  
  void run();
  std::string print();

  private:
  double val;

};

#endif /* INSTRUCTIONFan */
