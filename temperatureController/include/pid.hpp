#ifndef PID
#define PID

class PIDController {
  public:
  PIDController();
  ~PIDController();

  double refresh (double input, double target);
  void setParameters(double p, double i, double d);
  void reset();

  private:
  double param_p;
  double param_i;
  double param_d;
  double old_error;
  double integral;

};

#endif /* PID */
