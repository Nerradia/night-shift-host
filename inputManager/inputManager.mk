MAKE_DIR = $(PWD)

LIB := ../libs/inputManager.a

GENINCLUDE = ../include

MOTIONPLANNER_DIR			:= $(MAKE_DIR)/motionPlanner
UTILS_DIR				:= $(MAKE_DIR)/utils


SRCDIR   = src
INCLDIR  = include
OBJDIR   = ../obj
BINDIR   = bin

MANUAL_TEST_FILE = tests/main.cpp
MANUAL_TEST_OUTPUT_FILE = inputManagertest

SOURCES  := $(wildcard $(SRCDIR)/*.cpp)
INCLUDES := $(wildcard $(INCLDIR)/*.hpp)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
DEPENDS  := $(OBJECTS:.o=.d)


CC = g++
CFLAGS = $(ROOT_CFLAGS) -I$(INCLDIR) -I$(GENINCLUDE) $(INC_SRCH_PATH)

$(LIB): $(OBJECTS)
	@mkdir -p ../libs
	$(AR) rvs $@ $^
	@echo "    Archive    $(notdir $@)"


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CC) -c $^ $(CFLAGS) -o $@

.PHONY: clean
clean:
	@$(RM) -f $(LIB) $(OBJECTS)
	@$(RM) -f *.expand
	@echo "    Remove Objects:   $(OBJECTS)"

#To test the library with the tests/main.cpp file, please use make -f inputManager.mk test_m
.PHONY: test_m
test_m:
	$(CC) $(CFLAGS) $(MANUAL_TEST_FILE) -o $(MANUAL_TEST_OUTPUT_FILE)
