#ifndef FPGA_HPP__
#define FPGA_HPP__

#include <stdint.h>
#include <iostream>
#include <fstream>
#include "spi.hpp"

class FPGA_Reg {
  public:
  /* Register address */
  const uint16_t addr_offset;
};

class FPGA_Reg_string {
  public:
  /* Register address */
  uint16_t addr_offset;
  /* Word length */
  uint16_t  length;
};


class FPGA {
  public:
  /* Initializes the SPI connexion */
  int init(const char* spi_dev);

  void moveQueued(uint32_t clk_div, uint32_t max_steps, int32_t* steps, std::ofstream &f);
  void writeQueued(FPGA_Reg reg, uint32_t data);
  void write(FPGA_Reg reg, uint32_t data);
  uint32_t read(FPGA_Reg reg);

  std::string read(FPGA_Reg_string reg);

  /* FPGA-specific constants */
  static const int n_power_output    = 12; 
  static const int n_drivers         = 10; 
  static const int clk_frequency     = 47000000; 
  static const int n_triggers        = 8; 
  static const int n_trigger_inputs  = 16;


  const FPGA_Reg_string Build_version = {0x0000, 8};
  const FPGA_Reg_string Build_date    = {0x0008, 8};
  /* Debug vector TBD */
  const FPGA_Reg sync_register        = {0x00AA};
  const FPGA_Reg fifo_word_count      = {0x0028};

  class {
    public:
    const FPGA_Reg driver_enable      = {0x0400};
  } driver_controller;

  class {
    public:
    const FPGA_Reg spi_mux            = {0x0800};
  } spi_mux;

  class {
    public:
    const FPGA_Reg power_output[12]  = {{0x0C00},
                                        {0x0C01},
                                        {0x0C02},
                                        {0x0C03},
                                        {0x0C04},
                                        {0x0C05},
                                        {0x0C06},
                                        {0x0C07},
                                        {0x0C08},
                                        {0x0C09},
                                        {0x0C0A},
                                        {0x0C0B}};
    const FPGA_Reg watchdog_mask      = {0x0C0C};
  } power_output;

  class {
    public:
    const FPGA_Reg current_pos[10] =   {{0x1000},
                                        {0x1001},
                                        {0x1002},
                                        {0x1003},
                                        {0x1004},
                                        {0x1005},
                                        {0x1006},
                                        {0x1007},
                                        {0x1008},
                                        {0x1009}};

    const FPGA_Reg captured_pos[10] =  {{0x100A},
                                        {0x100B},
                                        {0x100C},
                                        {0x100D},
                                        {0x100E},
                                        {0x100F},
                                        {0x1010},
                                        {0x1011},
                                        {0x1012},
                                        {0x1013}};

    const FPGA_Reg trigger_in_sel[10] ={{0x1014},
                                        {0x1015},
                                        {0x1016},
                                        {0x1017},
                                        {0x1018},
                                        {0x1019},
                                        {0x101A},
                                        {0x101B}};

    const FPGA_Reg drv_sel[10] =       {{0x101C},
                                        {0x101D},
                                        {0x101E},
                                        {0x101F},
                                        {0x1020},
                                        {0x1021},
                                        {0x1022},
                                        {0x1023},
                                        {0x1024},
                                        {0x1025}};

    const FPGA_Reg drv_action[10] =    {{0x1026},
                                        {0x1027},
                                        {0x1028},
                                        {0x1029},
                                        {0x102A},
                                        {0x102B},
                                        {0x102C},
                                        {0x102D},
                                        {0x102E},
                                        {0x102F}};

    const FPGA_Reg trigger_reset      = {0x1030};
    const FPGA_Reg trigger_state      = {0x1031};
    const FPGA_Reg polarity_swap      = {0x1032};
    const FPGA_Reg position_reset     = {0x1033};
  } position_controller;

  class {
    public:
    const FPGA_Reg current_channel =    {0x1400};
    const FPGA_Reg mask_high_prio  =    {0x1401};
    const FPGA_Reg mask_low_prio   =    {0x1402};
    const FPGA_Reg adc_val[16]     =   {{0x1403},
                                        {0x1404},
                                        {0x1405},
                                        {0x1406},
                                        {0x1407},
                                        {0x1408},
                                        {0x1409},
                                        {0x140A},
                                        {0x140B},
                                        {0x140C},
                                        {0x140D},
                                        {0x140E},
                                        {0x140F},
                                        {0x1410},
                                        {0x1411},
                                        {0x1412}};
  } adc_reader;

  class {
    public:
    const FPGA_Reg servo[16]       =   {{0x1800},
                                        {0x1801},
                                        {0x1802},
                                        {0x1803},
                                        {0x1804},
                                        {0x1805},
                                        {0x1806},
                                        {0x1807}};
  } servo_controller;

  private:
  /* SPI commands */
  const char READ_IMMEDIATE  = 0x01;
  const char WRITE_IMMEDIATE = 0x02;
  const char WRITE_QUEUED    = 0x0A;
  const char STEPS_QUEUED    = 0x0B;


  Spi spi;
  
};
#endif /* FPGA */
