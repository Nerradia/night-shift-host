#ifndef HOST
#define HOST

#include <iostream>
#include <thread.hpp>
#include <queue>
#include <instruction.hpp>

class Host : public thread {
  public:
  Host();
  ~Host();

  void process();
  std::string threadName();
  void queueInstruction(Instruction* i);
  double getQueueSize();

  enum hostState {
    INIT,
    RUNNING,
    STOP
  };


  /* Wait until the printer is not moving anymore */
  void waitMotionPipelineEmpty();
  
  /* Flush pending moves, which sets the motion pipeline in an undefined state
     Host::syncMotionPipeline() must be called before moving again */
  void flushMotionPipeline();
  
  /* Resets the motion pipeline to working state after being flushed */
  void syncMotionPipeline();

  /* Get the last captured position of an axis */
  double getCapturedPosition(int axis);

  std::array<double, Config::n_axes> getAbsolutePosition();
  /* Direct moves that will be run right after being called */
  void moveAbsolute(std::array<double, Config::n_axes> target, double speed);
  void moveAbsolute(std::array<double, Config::n_axes> target);
  void moveRelative(std::array<double, Config::n_axes> target, double speed);
  void moveRelative(std::array<double, Config::n_axes> target);
  
  double getTargetAbsolute(int axis);
  /* Prepare a move setting targets axis per axis then run it with goToTarget() */
  void setTargetAbsolute(int axis, double target);
  
  /* Prepare a move setting targets axis per axis then run it with goToTarget() */
  void setTargetRelative(int axis, double target);
  
  /* Prepare a move setting targets axis per axis then run it with goToTarget() */
  void setSpeed(double speed);
  
  /* Send a move instruction to the pipeline */
  void goToTarget();

  /* Offset settings */
  std::array<double, Config::n_axes> getOffset();

  /* Offset settings */
  double getOffset(int axis);

  /* Sets the motion pipeline offset */
  void setCurrentPosition(int axis, double currentPosition);

  /* Sets the motion pipeline offset */
  void setCurrentPosition(std::array<double, Config::n_axes> currentPosition);

  /* Sets the extruder to use for following moves */
  void setCurrentExtruder(int id); 

  /* Sets the pressure advance constant, in s^-1 */
  void setPressureAdvance(double coef);

  void setBedLevelingOffsets(std::vector<std::vector<double>> map, int np, double tileSizeX, double tileSizeY, double offsetX, double offsetY);

  enum posMode {
      POS_RELATIVE,
      POS_ABSOLUTE
  };
  struct {
    /* Last position requested to host */
    std::array<double, Config::n_axes> current;
    /* Last speed requested */
    double speed;
    /* Axis offsets between requests and motion-planner */
    std::array<double, Config::n_axes> offset;

    /* Current mode for movements */
    enum posMode mode;
  } pos;


  /* Set the printer to stop an axis on an endstop event */
  void setEndstop(int axis);

  /* Set the printer to stop an axis and remember the position on endstop event */
  void setProbe(int axis);

  /* Get the state of the endstop event */
  bool getEndstopState(int axis);

  /* Reset endstop trigger */
  void resetEndstopState(int axis);
  
  /* Erase the event listening (which frees the FPGA's trigger channels) */
  void freeEndstop(int axis);

  /* Get the BLTouch ready and armed to probe */
  void armBLtouch();

  /* Get the BLTouch safe */
  void disarmBLtouch();

  /* Show a debugging message */
  void traceStr(std::string);
   
  /* Set an offset to a stepper that will be applied on the next move,
    it should be a slow move, this offset is applied after acceleration planning ! */
  void setStepperOffset(int stepperId, double offset);

  private:
  hostState hoststate;

  void move();
  void processRun();
  void processInit();
  void processStop();

  std::queue<Instruction*> instructionQueue;

  int currentExtruderId = -1; // Unkown extruder at start
};

extern Host host;

#endif /* HOST */
