
#include <instructionSetExtruder.hpp>
#include <instructionUtils.hpp>
#include <interpolation.hpp>
#include <host.hpp>
#include <cmath>

/* Mapping functions */
instructionDefinition InstructionSetExtruder::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionSetExtruder::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionSetExtruder::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("T0");
  list.push_back("T1");

  return list;
}

Instruction* InstructionSetExtruder::mapHook(std::string gcode) {
  return new InstructionSetExtruder(gcode);
}



InstructionSetExtruder::InstructionSetExtruder(std::string gcode) {

  auto gcodeData = instructionUtils::parseArguments(gcode);

  for(auto &key : gcodeData) {
    host.traceStr("Parameter parsed : " + key.first + " : " + std::to_string(key.second));
  }

  /* Check what gcode instruction was given */
  int gcodeNumber = std::round(gcodeData["T"]);
  switch(gcodeNumber) {
    case 0:
    case 1:
      extruderId = gcodeNumber;
      break;

    default:
      throw std::string("Requested an unknown tool !");
  }

  /* R flag to only switch head without moving material and stuff */
  if (gcodeData.find("R") != gcodeData.end()) {
    host.traceStr("R is here !");
    onlySwitch = true;
  }
  else {
    host.traceStr("R is NOT here !");
    onlySwitch = false;
  }

}

InstructionSetExtruder::~InstructionSetExtruder() {

}

std::string InstructionSetExtruder::print() {
  std::string s;
  s += "Switch to extruder " + std::to_string(extruderId);

  return s;
}

void InstructionSetExtruder::run() {
  if (!onlySwitch) {
    /* Remember where the gcode stops */
    auto position = host.getAbsolutePosition();

    /* Zero extruder position */
    host.setCurrentPosition(3, 0);

    /* Go up if Z axis is too low */
    if (position[2] < Config::nozzle_switch_pos[2]) {
      host.setTargetAbsolute(2, Config::nozzle_switch_pos[2]);
      host.setSpeed(Config::nozzle_switch_head_speed);
      host.goToTarget();
    }
    else {
      host.setTargetRelative(2, 0.001);
      host.setSpeed(Config::nozzle_switch_head_speed);
      host.goToTarget();
    }

    /* Move to dumping position */
    host.setTargetAbsolute(0, Config::nozzle_switch_pos[0]);
    host.setTargetAbsolute(1, Config::nozzle_switch_pos[1]);
    host.setSpeed(Config::nozzle_switch_head_speed);
    host.goToTarget();

    /* Extrude a bit */
    host.setTargetAbsolute(3, Config::nozzle_switch_prime_before);
    host.setSpeed(Config::nozzle_switch_primiming_speed);
    host.goToTarget();

    /* Zero extruder */
    host.setCurrentPosition(3, 0);

    /* Go to cooling position */
    host.setTargetAbsolute(3, -Config::nozzle_switch_cooling_pos);
    host.setSpeed(Config::nozzle_switch_retract_speed);
    host.goToTarget();

    /* Wait */
    host.waitMotionPipelineEmpty();
    std::this_thread::sleep_for(std::chrono::milliseconds((int) (1000. * Config::nozzle_switch_first_cooling_time)));

    /* Heat back the tip */
    host.setTargetAbsolute(3, 0);
    host.setSpeed(Config::nozzle_switch_filament_speed);
    host.goToTarget();

    /* Go to cooling position */
    host.setTargetAbsolute(3, -Config::nozzle_switch_cooling_pos);
    host.setSpeed(Config::nozzle_switch_retract_speed);
    host.goToTarget();

    /* Wait for cooldown */
    host.waitMotionPipelineEmpty();
    std::this_thread::sleep_for(std::chrono::milliseconds((int) (1000. * Config::nozzle_switch_second_cooling_time)));


    /* Retract */
    host.setTargetAbsolute(3, -Config::nozzle_switch_retracted_pos - Config::nozzle_switch_prime_before);
    host.setSpeed(Config::nozzle_switch_retract_speed);
    host.goToTarget();

    /* Block until everything planned is done */
    host.waitMotionPipelineEmpty();

    /* Switch extruder */
    host.setCurrentExtruder(extruderId);

    /* Feed in the filament, first going fast then slowly when entering the "variability zone" */
    host.setTargetAbsolute(3, -Config::nozzle_switch_safe_length/2);
    host.setSpeed(Config::nozzle_switch_filament_speed);
    host.goToTarget();

    /* Extrude to purge the nozzle from old filament */
    host.setTargetAbsolute(3, Config::nozzle_switch_safe_length);
    host.setSpeed(Config::nozzle_switch_primiming_speed);
    host.goToTarget();

    /* Go back to the gcode position */
    host.setTargetAbsolute(0, position[0]);
    host.setTargetAbsolute(1, position[1]);
    host.setTargetAbsolute(2, position[2]);
    host.setSpeed(Config::nozzle_switch_head_speed);
    host.goToTarget();

    /* Reset extruder position to the gcode's */
    host.setCurrentPosition(3, position[3]);
  }
  else {
    /* Switch extruder */
    host.setCurrentExtruder(extruderId);
  }

}
