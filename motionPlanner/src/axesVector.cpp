
#include <axesVector.hpp>
#include <cmath>


AxesVector::AxesVector(std::array<double, Config::n_axes> &v) {

  for (int i = 0; i < this->size(); i++) {
    this->at(i) = v.at(i);
  }

  //*this = v;
}

AxesVector::AxesVector() {
}

AxesVector AxesVector::operator=(std::array<double, Config::n_axes> &in) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = in[i];
  }
  return out;
}

AxesVector AxesVector::operator+(AxesVector &a) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = this->at(i) + a[i];
  }
  return out;
}

AxesVector AxesVector::operator-(AxesVector &a) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = this->at(i) - a[i];
  }
  return out;
}

AxesVector AxesVector::operator*(AxesVector &a) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = this->at(i) * a[i];
  }
  return out;
}

AxesVector AxesVector::operator*(double a) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = this->at(i) * a;
  }
  return out;
}

AxesVector AxesVector::operator/(double a) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = this->at(i) / a;
  }
  return out;
}

AxesVector operator+(const AxesVector &a, const AxesVector &b) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = a[i] + b[i];
  }
  return out;
}

AxesVector operator-(const AxesVector &a, const AxesVector &b) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = a[i] - b[i];
  }
  return out;
}

AxesVector operator/(const AxesVector &a, const AxesVector &b) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out[i] = a[i] / b[i];
  }
  return out;
}

double AxesVector::sum() {
  double out;
  for (int i = 0; i < Config::n_axes; i++) {
    out += this->at(i);
  }
  return out;
}


double AxesVector::norm() {
  double out = 0;
  for (int i = 0; i < Config::n_axes; i++) {
    out += std::pow(this->at(i), 2);
  }
  return std::sqrt(out);
}

std::string AxesVector::print() {
  std::string s;
  s += "[";
  for (int i = 0; i < Config::n_axes; i++) {
    if (i > 0) {
      s += ", ";
    }
    s += std::to_string(this->at(i));
  }
  s += "]";
  return s;
}

double max(const AxesVector &a) {
  double out = a.at(0);
  for (int i = 0; i < Config::n_axes; i++) {
    if (a.at(i) > out) {
      out = a.at(i);
    }
  }
  return out;
}

AxesVector abs(const AxesVector &a) {
  AxesVector out;
  for (int i = 0; i < Config::n_axes; i++) {
    out.at(i) = std::abs(a.at(i));
  }
  return out;
}