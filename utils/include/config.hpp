#ifndef CONFIG_HPP__
#define CONFIG_HPP__

#include <map>
#include <array>
#include <vector>

class Config {

  public:
  
  Config();

  static constexpr double Ts = 0.001;

  static constexpr double interpMaxLength = 0.005;
  static constexpr double interpMaxRatio = 10;
  static constexpr double interpMaxAngle = (30./180.*3.1415926535897932384626433832795);


  static constexpr int n_axes = 4;
  static constexpr int n_motors = 10; // TODO: redundant with the FPGA class's constant

  static std::array<std::vector<int>, n_axes> axisToStepper;
  static std::array<int, n_axes> axisHomingEndstop;
  static std::array<int, n_axes> axisHomingChannel;
  
  static std::array<double, n_axes> axisMaxSpeed;
  static std::array<double, n_axes> axisMaxAcceleration;
  static constexpr int motionPlannerInputQueueSize = 20;
  static constexpr int motionPlannerAccelQueueSize = 200;
  static constexpr int motionPlanneroutputQueueSize = 2000;

  static std::array<int, Config::n_axes> absolute_axes;
  static std::array<double, n_motors> stepperResolution;
  static std::vector<int> homing_axes;
  static constexpr int probing_axis = 2; // Z homes with BLtouch
  static std::array<double, Config::n_axes> probing_point; // Where should the Z homing be done

  /* Where is the head when homed */
  static std::array<double, n_axes> homing_position;

  /* This is a distance that will sent to the motion planner as is to move to 
     home, it should be a bit longer than the axis's length */
  static std::vector<double> homing_direction;

  static constexpr double default_speed = 0.01;

  static std::map<int, std::string> axisToGcodeMap;

  static constexpr int instructionQueueSize = 100;
  static constexpr int simu = 0;
  static constexpr bool traceAllToFile = true;

  static constexpr double homing_speed_XY = 0.15;
  static constexpr double homing_speed_Z = 0.02;

  /* Number of points per axis for bed leveling */
  static constexpr int    bedleveling_n_points = 3;
  static constexpr double bedleveling_speed_travel   = 0.4;
  static constexpr double bedleveling_speed_probing  = 0.005;
  static constexpr double bedleveling_safe_height    = 0.02;
  static constexpr double bedleveling_probing_height =  0.005;
  static constexpr double bedleveling_probing_target   = -0.003;
  
  static constexpr double bedleveling_probe_offset_x = -0.01;
  static constexpr double bedleveling_probe_offset_y =  0.03;

  static constexpr int n_heaters = 2;
  static constexpr double f_heater0 = 2;
  static constexpr double f_heater1 = 1;

  static constexpr double pid_integral_max = 0.8;

  static constexpr double printersize_x = 0.465;
  static constexpr double printersize_y = 0.375;

  static constexpr double bltouch_offset = 0.0020;//0.00305;



  /* Where must the head be when priming filament */
  static std::array<double, n_axes> nozzle_switch_pos;

  static constexpr double nozzle_switch_head_speed = 0.1; // 1 m/s
  
  /* Speed for pulling out filament from the hotend */
  static constexpr double nozzle_switch_retract_speed = 0.040;

  /* High-speed for putting back the filament into the hotend */
  static constexpr double nozzle_switch_filament_speed = 0.020;

  /* Low-speed for putting back the filament and priming */
  static constexpr double nozzle_switch_primiming_speed = 0.006;

  /* Length between retracted filament and active filament position */
  static constexpr double nozzle_switch_retracted_pos = 0.300; // 300 mm for the top of the Y splitter
  
  /* Length between retracted filament and active filament position */
  static constexpr double nozzle_switch_cooling_pos = 0.075; // 300 mm for the top of the Y splitter

  /* How much filament will be primed to account for filament position variability and replacing melted plastic in the nozzle */
  static constexpr double nozzle_switch_safe_length = 0.080;

  /* How much filament will be primed before swapping */
  static constexpr double nozzle_switch_prime_before = 0.030;
  
  /* Cooling time */
  static constexpr double nozzle_switch_first_cooling_time = 8; // 300 mm for the top of the Y splitter
  static constexpr double nozzle_switch_second_cooling_time = 8; // 300 mm for the top of the Y splitter
};

extern Config config;

#endif
