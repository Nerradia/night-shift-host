MAKE_DIR = $(PWD)

MOTIONPLANNER_DIR		:= $(MAKE_DIR)/motionPlanner
INSTRUCTIONS_DIR		:= $(MAKE_DIR)/instructions
TEMPMANAGER_DIR			:= $(MAKE_DIR)/temperatureController
INPUTMANAGER_DIR		:= $(MAKE_DIR)/inputManager
PRINTER_DIR					:= $(MAKE_DIR)/printer
INCLUDE_DIR					:= $(MAKE_DIR)/include
WSINTERFACE_DIR  		:= $(MAKE_DIR)/wsInterface
UTILS_DIR						:= $(MAKE_DIR)/utils

EIGEN := $(MAKE_DIR)/submodules/eigen
FTXUI := $(MAKE_DIR)/submodules/FTXUI
WEBSOCKETPP := $(MAKE_DIR)/submodules/websocketpp

INC_SRCH_PATH := 
INC_SRCH_PATH += -I$(MOTIONPLANNER_DIR)/include
INC_SRCH_PATH += -I$(INSTRUCTIONS_DIR)/include
INC_SRCH_PATH += -I$(TEMPMANAGER_DIR)/include 
INC_SRCH_PATH += -I$(INPUTMANAGER_DIR)/include 
INC_SRCH_PATH += -I$(PRINTER_DIR)/include
INC_SRCH_PATH += -I$(WSINTERFACE_DIR)/include
INC_SRCH_PATH += -I$(UTILS_DIR)/include
INC_SRCH_PATH += -I$(EIGEN)/
INC_SRCH_PATH += -I$(FTXUI)/include
INC_SRCH_PATH += -I$(WEBSOCKETPP)/
INC_SRCH_PATH += -I$(INCLUDE_DIR)

LIB_SRCH_PATH :=
LIB_SRCH_PATH += $(MAKE_DIR)/libs


LIBS := $(LIB_SRCH_PATH)/*.a
LIBS += $(FTXUI)/build/*.a

CC = g++

LD = ld

LINT = splint

#LIBS := -ldriver -ldebug -lmw -lm -lpthread

LDFLAGS :=

# define ASIO_STANDALONE for websocketpp
ROOT_CFLAGS = -lpthread  -Wall -std=c++17 -Wno-psabi -D ASIO_STANDALONE
export ROOT_CFLAGS

CFLAGS := $(ROOT_CFLAGS)
CFLAGS += $(INC_SRCH_PATH) 

export MAKE_DIR CC LD CFLAGS LDFLAGS LIBS LINT INC_SRCH_PATH EIGEN

all:
	@$(MAKE) -C utils -f utils.mk
	@$(MAKE) -C motionPlanner -f motionPlanner.mk
	@$(MAKE) -C wsInterface -f wsInterface.mk
	@$(MAKE) -C temperatureController -f temperatureController.mk
	@$(MAKE) -C inputManager -f inputManager.mk
	@$(MAKE) -C printer -f printer.mk
	@$(MAKE) -C instructions -f instructions.mk
	@$(MAKE) -C root -f root.mk

.PHONY: clean
clean:
	@$(MAKE) -C utils -f utils.mk clean
	@$(MAKE) -C motionPlanner -f motionPlanner.mk clean
	@$(MAKE) -C wsInterface -f wsInterface.mk clean
	@$(MAKE) -C temperatureController -f temperatureController.mk clean
	@$(MAKE) -C inputManager -f inputManager.mk clean
	@$(MAKE) -C printer -f printer.mk clean
	@$(MAKE) -C instructions -f instructions.mk clean
	@$(MAKE) -C root -f root.mk clean

	
.PHONY: motionPlanner
motionPlanner:
	@$(MAKE) -C motionPlanner -f motionPlanner.mk 
