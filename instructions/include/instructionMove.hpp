#ifndef INSTRUCTIONMOVE
#define INSTRUCTIONMOVE

#include <instruction.hpp>
#include <string>

class InstructionMove : public Instruction {
  public:
  InstructionMove(std::string gcode);
  ~InstructionMove();

  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();

  void run();
  std::string print();

  private:
  /* List of gcode instructions accepted by this instruction */
  static std::vector<std::string> getGcodeTokens();
  /* The mappable function to be called when a new instance is needed */
  static Instruction* mapHook(std::string gcode);

  std::map<std::string, double> gcodeData;
  

};

#endif /* INSTRUCTIONMOVE */
