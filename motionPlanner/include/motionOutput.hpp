#ifndef MOTIONOUTPUT
#define MOTIONOUTPUT

#include <thread.hpp>
#include <printer.hpp>
#include <safeQueue.hpp>
#include <move.hpp>
#include <moveSample.hpp>
#include <vector>
#include <array>

class MotionOutput : public thread {
  public:
    std::string threadName();
    MotionOutput(SafeQueue<Move> *outputQueue);
    ~MotionOutput();
    void process();

    void enQueue(PrinterMove in);

    /* Read the FPGA's position counters */
    void resync();

    /* Get the known position from steps counts */
    AxesVector getPosition();
    double getPosition(int axis);

    void flush();
    double getQueueSize();

    /* Set an offset to a stepper that will be applied on the next move,
      it should be a slow move, this offset is applied after acceleration planning ! */
    void setStepperOffset(int stepperId, double offset);

    /* Set the bed leveling offset map */
    void setBedLevelingOffsets(std::vector<std::vector<double>> map, int np, double tileSizeX, double tileSizeY, double offsetX, double offsetY);

    /* Set the linear advance factor */
    void setLinearAdvance(double factor);

  private:
    FILE* debugLogFile;
    std::vector<MoveSample> sampleMove(Move &m);
    PrinterMove toPrinterSteps(MoveSample move);
    std::mutex m;
    SafeQueue<Move> *outputQueue;
    bool flushRequest;

    std::array<int64_t, Printer::n_motors> steppers_position;

    /* Offsets for calibration of Z axis */
    std::array<double, Config::n_motors> stepperOffset;

    /* Offsets for bed distortion compensation */
    std::vector<std::vector<double>> bedLevelingMap;
    int bedLevelingSize;
    double bedLevelingTileSizeX;
    double bedLevelingTileSizeY;
    double bedLevelingTileOffsetX;
    double bedLevelingTileOffsetY;

    /* Distort head's time and space to follow the bed's deformations */
    AxesVector bedLeveling(AxesVector in);
    
    /* Interpolate a value in a square of size 1 */
    double bilinearInterp(double Q11, double Q12, double Q21, double Q22, double x, double y);

    /* Apply the linear advance to extruder axis */
    MoveSample linearAdvance(MoveSample in, AxesVector normalized);
    public: double linearAdvance_factor;
};


extern MotionOutput motionOutput;

#endif /* MOTIONOUTPUT */
