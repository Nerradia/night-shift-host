#ifndef INSTRUCTIONBEDLEVELING
#define INSTRUCTIONBEDLEVELING
#include <instruction.hpp>
#include <string>

class InstructionBedLeveling : public Instruction {
  public:
  InstructionBedLeveling(std::string gcode);
  ~InstructionBedLeveling();

  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);
  
  void run();
  std::string print();

  private:
  
  class ProbePoint {
    public:
    double X;
    double Y;
    double Z;
  };

  void probePoint(class ProbePoint &p);
  std::vector<ProbePoint> probeArea(int nPoints);

};

#endif /* INSTRUCTIONBEDLEVELING */
