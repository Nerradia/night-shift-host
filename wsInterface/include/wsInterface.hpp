#ifndef WSINTERFACE
#define WSINTERFACE


#include <thread.hpp>
#include <string>
#include <pid.hpp>
#include <config.hpp>
#include <fstream>

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <iostream>
typedef websocketpp::server<websocketpp::config::asio> server;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

// pull out the type of messages sent by our config
typedef server::message_ptr message_ptr;


class WsInterface : public thread {
public:
  WsInterface();
  ~WsInterface();

  void process();
  std::string threadName();

private:


};


extern WsInterface wsInterface;

#endif /* WSINTERFACE */
