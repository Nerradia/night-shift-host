#ifndef SAFEQUEUE
#define SAFEQUEUE

#include <queue>
#include <mutex>
#include <thread>

/* Thread-safe wrapper for std::queue */
template <class QueueType>
class SafeQueue {
  public:

  auto size() {
    m.lock();
    auto ret = q.size();
    m.unlock();
    return ret;
  }

  /* Blocking pop */
  auto pop() {
    m.lock();
    while (q.empty()) {
      m.unlock();
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      m.lock();
    }
    auto ret = q.front();
    q.pop();
    m.unlock();
    return ret;
  }

  /* Blocking get (reads but doesn't remove from queue) */
  auto get() {
    m.lock();
    while (q.empty()) {
      m.unlock();
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      m.lock();
    }
    auto ret = q.front();
    m.unlock();
    return ret;
  }

  auto push(QueueType &in) {
    m.lock();
    q.push(in);
    m.unlock();
  }

  void flush() {
    m.lock();
    std::queue<QueueType>().swap(q);
    m.unlock();
  }

  private:
  std::mutex m;
  std::queue<QueueType> q;
};


#endif /* SAFEQUEUE */
