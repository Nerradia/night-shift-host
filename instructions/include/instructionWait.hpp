#ifndef INSTRUCTIONWAIT
#define INSTRUCTIONWAIT

#include <instruction.hpp>
#include <string>

class InstructionWait : public Instruction {
  public:
  InstructionWait(std::string gcode);
  ~InstructionWait();
  
  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);

  void run();
  std::string print();

  private:
  double time_millis;

};

#endif /* INSTRUCTIONWait */
