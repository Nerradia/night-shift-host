#include <telnetInput.hpp>
#include <instruction.hpp>
#include <host.hpp>
#include <gcodeParser.hpp>

#define PORT 4562
/* TODO: Use a C++ way of making sockets */
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

static char header[] = "\r"
    "  _   _ _       _     _    _____ _     _  __ _   _    _           _   \r\n"
    " | \\ | (_)     | |   | |  / ____| |   (_)/ _| | | |  | |         | |  \r\n"
    " |  \\| |_  __ _| |__ | |_| (___ | |__  _| |_| |_| |__| | ___  ___| |_ \r\n"
    " | . ` | |/ _` | '_ \\| __|\\___ \\| '_ \\| |  _| __|  __  |/ _ \\/ __| __|\r\n"
    " | |\\  | | (_| | | | | |_ ____) | | | | | | | |_| |  | | (_) \\__ \\ |_ \r\n"
    " |_| \\_|_|\\__, |_| |_|\\__|_____/|_| |_|_|_|  \\__|_|  |_|\\___/|___/\\__|\r\n"
    "           __/ |                                                      \r\n"
    "          |___/                                                       \r\n";


TelnetInput::TelnetInput() {

}

TelnetInput::~TelnetInput() {

}

void TelnetInput::process() {

  /* Listening socket */
  SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock == INVALID_SOCKET) {
      throw std::string("socket() : " + std::to_string(errno));
  }

  SOCKADDR_IN sin = { 0 };
  sin.sin_addr.s_addr = htonl(INADDR_ANY); /* nous sommes un serveur, nous acceptons n'importe quelle adresse */
  sin.sin_family = AF_INET;
  sin.sin_port = htons(PORT);

  if(bind (sock, (SOCKADDR *) &sin, sizeof sin) == SOCKET_ERROR) {
    throw std::string("bind() : " + std::to_string(errno));
  }

  if(listen(sock, 1) == SOCKET_ERROR) {
    throw std::string("listen() : " + std::to_string(errno));
  }

  while(!stop_request) {

    gcodeParser gcodeparser;

    /* Accept a client */
    SOCKADDR_IN csin = { 0 };
    SOCKET csock;
    int sinsize = sizeof csin;
    csock = accept(sock, (SOCKADDR *)&csin, (socklen_t*) &sinsize);

    if(csock == INVALID_SOCKET) {
      throw std::string("accept() : " + std::to_string(errno));
    }

    trace("Accepted a client");

    send(csock, header, strlen(header), MSG_NOSIGNAL);

    send(csock, ">\r\n", 6, MSG_NOSIGNAL);

    while (1) {
      char rxbuffer[2048];
      int sz = recv(csock, rxbuffer, 2048, MSG_NOSIGNAL);
      if (sz <= 0) {
        /* Client left */
        trace("Client left");
        break;
      }

      /* Terminate */
      rxbuffer[sz] = 0;

      /* Convert to C++ */
      auto line = std::string(rxbuffer);
      
      trace("Input : " + std::string(rxbuffer));
      
      std::string resp;
      
      if (sz > 1) {
        /* Parse the line */
        Instruction* instruction;
        instruction = gcodeparser.parse(line);
        
        /* Queue the instruction if one was instanciated */
        /* Host thread will delete it once used */
        if (instruction != nullptr) {
          trace("Queued " + instruction->print());
          resp = "Queued " + instruction->print();
          host.queueInstruction(instruction);
        }
        else {
          trace("Skipping line : " + line);
          resp = "Could not parse !";
        }

        resp += "\r\n> ";
        send(csock, resp.c_str(), resp.length() + 1, MSG_NOSIGNAL);

      }
    }

  }

  closesocket(sock);

}

std::string TelnetInput::threadName() {
  return "Telnet Input";
}

