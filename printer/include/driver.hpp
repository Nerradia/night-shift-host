#ifndef DRIVER_HPP__
#define DRIVER_HPP__

#include <spi.hpp>

/* TODO: Make or use a real library for TMC2130's */
class Driver {
  public:

  Driver();
  ~Driver();

  void setSpi(Spi* in);

  void init(double RMSAmps);

  private:
  Spi* spi;
  void sendTMC(char reg, uint32_t data);
};

#endif
