#ifndef BLTOUCH
#define BLTOUCH

class BLTouch {

  public:
  BLTouch();
  ~BLTouch();

  void init(int servoId, int endstopId);
  void reset();
  void probe();
  void retract();
  void selfTest();
  int getEndstopId();

  private:
  int servoId;
  int endstopId;

};

#endif /* BLTOUCH */
