PROG = $(MAKE_DIR)/bin/night-shift

MAKE_DIR = $(PWD)

SRCDIR = $(MAKE_DIR)/src
INCLDIR = $(MAKE_DIR)/include

SRCS = $(wildcard $(SRCDIR)/*.cpp)
GENINCLUDE = ../include


all :
	@mkdir -p ../bin
	$(CC) $(SRCS) $(LIBS) -o $(PROG) $(CFLAGS) -I$(GENINCLUDE) $(INC_SRCH_PATH)
	@echo "  \033[92m Generate Program $(notdir $(PROG)) from $(SRCS) to $(PROG) \033[0m"

.PHONY: clean
clean:
	@$(RM) -f $(OBJS) $(PROG)
	@$(RM) -f *.expand
	@$(RM) -rf ../prog ../libs
	@echo "    Remove Objects:   $(OBJS)"
	@echo "    Remove Libraries:  $(notdir $(PROG))"