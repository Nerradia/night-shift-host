
#include <accelerationPlanner.hpp>
#include <cmath>

std::string AccelerationPlanner::threadName() {
  return std::string("Acceleration planner");
}

AccelerationPlanner::~AccelerationPlanner() {

}

AccelerationPlanner::AccelerationPlanner(SafeVector<Move> *accelQueue, SafeQueue<Move> *outputQueue) {
  this->accelQueue = accelQueue;
  this->outputQueue = outputQueue;
  flushRequest = false;
}

void AccelerationPlanner::flush() {
  flushRequest = true;
}

void AccelerationPlanner::process() { 
  
  /* How much time is affected by the "assumed end" of the travel, which should
     be run only if we are starving */
  auto decelerationTime = AxesVector(Config::axisMaxSpeed) / AxesVector(Config::axisMaxAcceleration);
  maxDecelerationTime = max(decelerationTime);
  trace("Maximum speeds : " +  AxesVector(Config::axisMaxSpeed).print());
  trace("Maximum accelerations : " +  AxesVector(Config::axisMaxAcceleration).print());
  trace("Deceleration time for each axis : " + decelerationTime.print());
  trace("maxDecelerationTime = " + std::to_string(maxDecelerationTime));

  /* At what speed was the previous move */
  Move lastQueuedMove;
  lastQueuedMove.endSpeed = 0;

  /* Dummy move with a null speed */
  Move stillMove;
  stillMove.origin.fill(-2);
  stillMove.target.fill(-1);
  stillMove.targetSpeed = 0;

  lastQueuedMove = stillMove;

  bool coldStart = true;

  while(!stop_request) {

    /* Wait for at least one segment */
    auto inputSize = accelQueue->size();
    while (inputSize < 1) {
      inputSize = accelQueue->size();
      status = E_STATUS_IDLE;
      flushRequest = false;
      coldStart = true; // TODO: Check if it's right
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    /* handle flush requests */
    if (flushRequest) {
      while(accelQueue->size() > 0) {
        accelQueue->erase(0);
      }
      continue;
    }

    /* If output queue is full, wait */
    while (outputQueue->size() > Config::motionPlanneroutputQueueSize) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      status = E_STATUS_IDLE;
    }

    status = E_STATUS_RUNNING;

    trace("New acceleration planning batch started, input queue size : " + std::to_string(inputSize));

    /* How many moves are affected by the "assumed end" of the travel, which
       should be run only if we are starving */
    auto reservedMoves = howManySamplesForTime(maxDecelerationTime);
    trace("reservedMoves = " + std::to_string(reservedMoves));

    /* How many moves must be acceleration-planned */
    unsigned int movesToPlanify = inputSize;

    /* How many moves are not affected by the deceleration from "assumed end" */
    unsigned int movesToQueue;
    if (inputSize <= reservedMoves) {
      /* Not enough moves, we are starving */
      setStatusString("Throttling");
      movesToQueue = 1;
    } else {
      setStatusString("OK");
      movesToQueue = inputSize - reservedMoves;
    }

    trace("movesToQueue = " + std::to_string(movesToQueue));
    trace("movesToPlanify = " + std::to_string(movesToPlanify));

    /* 1st step : 
       Set all speeds to target speed */
    for (int i = 0; i < movesToPlanify; i++) {
      double speed = accelQueue->at(i).targetSpeed;
      accelQueue->at(i).startSpeed = speed;
      accelQueue->at(i).endSpeed = speed;
    }

    /* 2nd step : planify accelerations */
    /* First move's input speed depends on what was sent last iteration 
       (or zero when we were stopped)*/
    if (coldStart) {
      accelQueue->at(0).startSpeed = 0;
      plannifyAcceleration(stillMove, accelQueue->at(0));
    } else {
      plannifyAcceleration(lastQueuedMove, accelQueue->at(0));
    }

    for (int i = 0; i < movesToPlanify-1; i++) {
      plannifyAcceleration(accelQueue->at(i), accelQueue->at(i+1));
    }
    /* Last move's end speed is always zero */
    accelQueue->at(movesToPlanify-1).endSpeed = 0;

    /* 3rd step : planify decelerations */
    for (int i = 1; i < movesToPlanify; i++) {
      plannifyDeceleration(accelQueue->at(movesToPlanify - i -1), accelQueue->at(movesToPlanify - i));
    }

    /* 4th step : send to the last thread */ 
    for (int i = 0; i < movesToQueue; i++) {
      outputQueue->push(accelQueue->at(0));
      lastQueuedMove = accelQueue->at(0); // TODO: write that only once
      accelQueue->erase(0);
    }

    coldStart = false;
  }
}

void AccelerationPlanner::plannifyAcceleration(Move &a, Move &b) {
  
  AxesVector axesTangentDiff = a.getEndTangent() - b.getStartTangent();

  trace(a.print() + " and " + b.print());

  /* Step 1 : Find start speed of b */

  /* See if moves are tangent. */
  if (axesTangentDiff.norm() > 0.1) {
    /* Not tangent, set the junction speed to zero because we can't change
      direction with a non-null speed while respecting acceleration 
      constraints */
     trace("Not tangent : " + a.getEndTangent().print() + b.getStartTangent().print());
    b.startSpeed = 0;
  }
  else {
    /* Tangent, equalize b's start speed with a's end speed (if not already 
       lower for some other reason) */
    b.startSpeed = std::min(a.endSpeed, b.startSpeed);
    trace("Tangent at speed : " + std::to_string(b.startSpeed));
  }

  /* Step 2 : Find end speed of b */
  /* For each axis that moves, calculate the maximum speed it can go */

  /* Beginning speed per axis */
  AxesVector bStartAxesSpeed = b.getStartTangent() * b.startSpeed;
  
  for (int i = 0; i < Config::n_axes; i++) {
    /* Calculate only if axis is moving */
    if (std::abs(b.relative(i)) > 0) {

      /* acceleration = (delta V) / (delta t);
         delta t = d / ((Vin + Vout) / 2);
         acceleration = (Vout - Vin) / (2d / (Vin + Vout));
      => Vout = sqrt(2 * acceleration * d + Vin²); */
      double axisMaxSpeed = std::sqrt(2 * Config::axisMaxAcceleration[i] * std::abs(b.relative(i)) + std::pow(bStartAxesSpeed[i], 2));

      double maxCombinedSpeedForThisAxis = axisMaxSpeed / std::abs(b.getNormalized()[i]);

      /* Lower the combined speed if needed */
      b.endSpeed = std::min(b.endSpeed, maxCombinedSpeedForThisAxis);
    }
  }
}

void AccelerationPlanner::plannifyDeceleration(Move &a, Move &b) {

  /* Step 1 : Find end speed of a */

  /* Equalize b's start speed with a's end speed (if not already lower for some
    other reason) */
  a.endSpeed = std::min(a.endSpeed, b.startSpeed);

  /* Step 2 : Find start speed of a */
  /* For each axis that moves, calculate the maximum speed it can go */

  /* Ending speed per axis */
  AxesVector aEndAxesSpeed = a.getEndTangent() * a.endSpeed;
  
  for (int i = 0; i < Config::n_axes; i++) {
    /* Calculate only if axis is moving */
    if (std::abs(a.relative(i)) > 0) {

      /* Same formula as in acceleration but "time is reversed" */
      double axisMaxSpeed = std::sqrt(2 * Config::axisMaxAcceleration[i] * std::abs(a.relative(i)) + std::pow(aEndAxesSpeed[i], 2));

      double maxCombinedSpeedForThisAxis = axisMaxSpeed / std::abs(a.getNormalized()[i]);

      /* Lower the combined speed if needed */
      a.startSpeed = std::min(a.startSpeed, maxCombinedSpeedForThisAxis);
    }
  }
}

unsigned int AccelerationPlanner::howManySamplesForTime(double time) {
  
  int i;
  for (i = 0; i < accelQueue->size() && time > 0; i++) {

    time -= accelQueue->at(i).getTargetTime();

  }

  return i;
}

