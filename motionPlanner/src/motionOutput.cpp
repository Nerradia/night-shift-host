#include <motionOutput.hpp>
#include <cmath>
#include <moveSample.hpp>
#include <printer.hpp>

int64_t doubleToQueuedTime(double in) {
  return in * 1000000;
}

std::string MotionOutput::threadName() {
  return std::string("MotionOutput");
}

MotionOutput::~MotionOutput() {
}

MotionOutput::MotionOutput(SafeQueue<Move> *outputQueue) {
  this->outputQueue = outputQueue;

  steppers_position.fill(0);

  debugLogFile = fopen("./motionOutput.csv", "w");
  if (debugLogFile == NULL) {
    throw std::string("Error opening file /motionOutput.csv !");
  }

  stepperOffset.fill(0);

  bedLevelingSize = 0;

  linearAdvance_factor = 0;

  flushRequest = false;
}

void MotionOutput::process() {
  /* Todo : remove */
  AxesVector old;
  int id = 0;

  while (!stop_request) {
        
    if (outputQueue->size() == 0) {
      status_string = "Starved";
      status = E_STATUS_IDLE;
      flushRequest = false;
      continue;
    }

    if (flushRequest) {
      outputQueue->flush();
      continue;
    }

    status = E_STATUS_RUNNING;
    status_string = "Queuing";
    auto m = outputQueue->pop();

    /* Process sample */
    auto samples = sampleMove(m);

    int nSamples = samples.size();

    //while(samples.size() > 0 && !flushRequest) {
    for (int i = 0; i < nSamples && !flushRequest; i++) {
      //auto s = samples.front();
      //samples.erase(samples.begin());

      auto s = samples.at(i);

      //double length = (s.target - old).norm();
      //double time = length / s.speed;

      //fprintf(debugLogFile, "%.16f, %.16f, %.16f, %.16f, %.16f, %d\n", s.target[0], s.target[1], s.target[2], s.target[3], s.speed, id);
     // fflush(debugLogFile);
      
      /* simulation sleep */
      //std::this_thread::sleep_for(std::chrono::milliseconds((int) (time * 1000.)));
      //old = s.target;

      auto stepperized = toPrinterSteps(s);

      int stepTotal = 0;
      for (int i = 0; i < Config::n_motors; i++) {
        stepTotal += std::abs(stepperized.target[i]);
      }
      if (stepTotal > 0) {
        printer.queueMove(stepperized);
      } else {
        trace("Skipped empty move");
      }
    }

    id++;

  }
}

std::vector<MoveSample> MotionOutput::sampleMove(Move &m) {

  trace("Sampling " + m.print());
  trace("Bezier control points " + m.bezier_p1.print() + m.bezier_p2.print());

  std::vector<MoveSample> out;

  /* Move's maximum speed before sampling (limits that don't change over time) */
  double speedLimit = m.targetSpeed;

  auto normalized = m.getNormalized();
  
  /* Compute the maximum speed allowed if we look at each axis's max speed */
  for (int i = 0; i < Config::n_axes; i++) {
    /* Calculate only if axis is moving */
    if (normalized[i] != 0.) {
      /* Maximum combined speed for this axis */
      double max = Config::axisMaxSpeed[i] / std::abs(normalized[i]);
      speedLimit = std::min(speedLimit, max);
    }
  }

  /* Combined acceleration based on the direction and axes's acceleration
     constraints */
  double acceleration = 987987;
  for (int i = 0; i < Config::n_axes; i++) {
    /* Calculate only if axis is moving */
    if (normalized[i] != 0.) {
      /* Maximum combined acceleration for this axis */
      double max = Config::axisMaxAcceleration[i] / std::abs(normalized[i]);
      acceleration = std::min(acceleration, max);
    }
  }

  if (acceleration == 987987) {
    throw std::string("No combined acceleration was computed !");
  }

  /* The distance traveled so far */
  double d = 0;

  /* Combined speed of the previous sample (initialized at move's startspeed but
     avoid 0) */
  double previous_speed = std::max(m.startSpeed, 0.001);

  trace("speedLimit : " + std::to_string(speedLimit)
   + " acceleration : " + std::to_string(acceleration)
   + " previous_speed : " + std::to_string(previous_speed)); 

  do {
    /* length that will be travelled by this move, determined by last sample's 
       speed and sample rate */
    double l = previous_speed * Config::Ts;

    /* Stop iterating if we exceed the move's length */
    if (d + l > m.length()) {
      break;
    }

    /* Determine speed of current sample which is the minimum of 3 speeds :
       - the starting speed + the speed gained by accelerating (full acceleration)
       - the speed we can go while going to the ending speed (full deceleration) 
       - the combined target speed (don't go faster than asked)
    */
    double speed = speedLimit;

    //trace("New sample : " + std::to_string(d) + " from origin, length " + std::to_string(l) + ", Bezier : " + std::to_string(m.bezier));

    /* acceleration */
    double maxSpeedAcc = std::sqrt(2 * acceleration * (d) + std::pow(m.startSpeed, 2));
    speed = std::min(speed, maxSpeedAcc);

    /* Deceleration */
    double maxSpeedDec = std::sqrt(2 * acceleration * (m.length() - d) + std::pow(m.endSpeed, 2));
    speed = std::min(speed, maxSpeedDec);

    //trace("Acceleration : " + std::to_string(d) + " from origin, deceleration : " + std::to_string(m.length() - d) + " to end");

    speed = std::max(speed, 0.01);

    speed = std::min(speed, speedLimit);

    /* The Bezier's function input, where 0 is the beginning and 1 the end of 
       the move*/
    double BezierParam = (d + l) / m.length();

    /* Determine end position of sample */
    AxesVector sampleEndPos = m.getSample(BezierParam);

    /* Store the sample after applying bed leveling and pressure-advance */
    out.push_back(linearAdvance(MoveSample(bedLeveling(sampleEndPos), speed), normalized));

    //trace("d = " + std::to_string(d) + "; speed = " + std::to_string(speed) + "; maxSpeedAcc = " + std::to_string(maxSpeedAcc) + "; maxSpeedDec = " + std::to_string(maxSpeedDec) + "; Endpos = " + sampleEndPos.print() + "; BezierParam = " + std::to_string(BezierParam));

    /* Rememer where we are */
    d = (sampleEndPos - m.origin).norm();
    //d += Config::Ts * speed;

    previous_speed = speed;

  } while(1);

  trace("Sampled into " + std::to_string(out.size()) + " elements");
  return out;
}

void MotionOutput::flush() {
  flushRequest = true;
}

void MotionOutput::resync() {

  /* Get the step FPGA's step counters of interesting axes (typically 
     not extruders that can overflow the FPGA's counters) */
  for (const auto axis : Config::absolute_axes) {
    for(const auto stepperId : Config::axisToStepper[axis]) {
      steppers_position[stepperId] = printer.getCurrentPos(stepperId);
      trace("Resync : Updated stepper " + std::to_string(stepperId) + " value : " + std::to_string(steppers_position[stepperId]));
    }
  }
}

AxesVector MotionOutput::getPosition() {
  AxesVector out;
  for (int axis = 0; axis < Config::n_axes; axis++) {
    int stepperId = Config::axisToStepper[axis].at(0);
    out[axis] = steppers_position[stepperId] * Config::stepperResolution[stepperId];
    trace("Resync : Axis " + std::to_string(axis) + " is at : " + std::to_string(out[axis]));
  }
  
  return out;
}

void MotionOutput::setStepperOffset(int stepperId, double offset) {
  stepperOffset[stepperId] = offset;
}

double MotionOutput::getPosition(int axis) {
  int stepperId = Config::axisToStepper[axis].at(0);
  return steppers_position[stepperId] * Config::stepperResolution[stepperId];
}

PrinterMove MotionOutput::toPrinterSteps(MoveSample move) {

  PrinterMove printerMove;
  printerMove.maxStepFreq = 0;

  /* Where we are exactly */
  std::array<double, Config::n_motors> current_position;

  /* Get our current position (in meters) from current step count for each axis */
  for (int stepperId = 0; stepperId < Config::n_motors; stepperId++) {
    current_position[stepperId] = steppers_position[stepperId] * Config::stepperResolution[stepperId];
  }

  /* Calculate the travel to do for each stepper, default to 0
     for unused steppers, don't forget to add steppers offsets */
  std::array<double, Config::n_motors> delta;
  delta.fill(0);

  for (int axis = 0; axis < Config::n_axes; axis++) {
    for(const auto stepperId : Config::axisToStepper[axis]) {
      delta[stepperId] = move.target[axis] - (current_position[stepperId] + stepperOffset[stepperId]);
    }
  }

  /* What stepper will do the biggest number of steps */
  int stepperIdMaxSteps = Config::axisToStepper[0][0];

  /* Convert to steps */
  for (int axis = 0; axis < Config::n_axes; axis++) {
    for(const auto stepperId : Config::axisToStepper[axis]) {
      /* Number of steps for this stepper */
      printerMove.target[stepperId] = delta[stepperId] / Config::stepperResolution[stepperId];

      if (std::abs(printerMove.target[stepperIdMaxSteps]) < std::abs(printerMove.target[stepperId])) {
        stepperIdMaxSteps = stepperId;
      }
    }
  }
  
  /* Step frequency of the stepper that makes the biggest travel,
    = (axis speed) / (stepper resolution) 
    = (normalized move * speed) / (stepper resolution) */

  /* Norm */
  double norm = 0;
  for (int i = 0; i < Config::n_motors; i++) {
    norm += std::pow(delta[i], 2);
  }
  norm = std::sqrt(norm);

  printerMove.maxStepFreq = std::abs((delta[stepperIdMaxSteps]) / norm * move.speed / Config::stepperResolution[stepperIdMaxSteps]);

  /* Update current position */
  for (int i = 0; i < Printer::n_motors; i++) {
    steppers_position[i] += printerMove.target[i]; 
  }

  return printerMove;
}

void MotionOutput::setBedLevelingOffsets(std::vector<std::vector<double>> map, int np, double tileSizeX, double tileSizeY, double offsetX, double offsetY) {

  bedLevelingMap = map;
  bedLevelingSize = np;
  bedLevelingTileSizeX = tileSizeX;
  bedLevelingTileSizeY = tileSizeY;
  bedLevelingTileOffsetX = offsetX;
  bedLevelingTileOffsetY = offsetY;

}

void MotionOutput::setLinearAdvance(double factor) {
  linearAdvance_factor = factor;
}

AxesVector MotionOutput::bedLeveling(AxesVector in) {
  if (bedLevelingSize == 0) {
    return in;
  }
  
  double X = in[0] - bedLevelingTileOffsetX;
  double Y = in[1] - bedLevelingTileOffsetY;

  //std::cout << "X : " << X << " Y : " << Y << std::endl;

  /* Avoid going out of the map */
  if (X < 0) X = 0;
  if (X > bedLevelingTileSizeX * bedLevelingSize) X = bedLevelingTileSizeX * bedLevelingSize;
  if (Y < 0) Y = 0;
  if (Y > bedLevelingTileSizeY * bedLevelingSize) Y = bedLevelingTileSizeY * bedLevelingSize;

  //std::cout << "X : " << X << " Y : " << Y << std::endl;

  /* Find on what tile of the map we are */
  int leftX = std::floor(X / bedLevelingTileSizeX);
  int bottomY = std::floor(Y / bedLevelingTileSizeY);
  int rightX = std::ceil(X / bedLevelingTileSizeX);
  int topY = std::ceil(Y / bedLevelingTileSizeY);

  if (leftX < 0) leftX = 0;
  if (bottomY < 0) bottomY = 0;
  if (rightX >= bedLevelingSize) rightX = bedLevelingSize -1;
  if (topY >= bedLevelingSize) topY = bedLevelingSize -1;

  //std::cout << "leftX : " << leftX << " bottomY : " << bottomY << " rightX : " << rightX << " topY : " << topY << std::endl;

  /* Where we are on that tile (normalized) */
  double x = X / bedLevelingTileSizeX - leftX;
  double y = Y / bedLevelingTileSizeY - bottomY;

  //std::cout << "x : " << x << " y : " << y << std::endl;

  double Q11, Q12, Q21, Q22;

  Q11 = bedLevelingMap[leftX][bottomY];
  Q12 = bedLevelingMap[leftX][topY];
  Q21 = bedLevelingMap[rightX][bottomY];
  Q22 = bedLevelingMap[rightX][topY];
  
  //std::cout << "Q11 : " << Q11 << " Q12 : " << Q12 << " Q21 : " << Q21 << " Q22 : " << Q22 << std::endl;

  double P = bilinearInterp(Q11, Q12, Q21, Q22, x, y); 

  in[2] += P;

  return in;
}

double MotionOutput::bilinearInterp(double Q11, double Q12, double Q21, double Q22, double x, double y) {

  double R1 = Q11 * (1 - x) + Q21 * x;
  double R2 = Q12 * (1 - x) + Q22 * x;
  double P  =  R1 * (1 - y) +  R2 * y;
  //std::cout << "R1 : " << R1 << " R2 : " << R2 << " P : " << P << std::endl; 
  return P;
}


MoveSample MotionOutput::linearAdvance(MoveSample in, AxesVector normalized) {
  //TODO: Make this configurable

  /* Calculate speed of the extruder axis */
  double es = std::max((in.speed - 0.015), 0.) * normalized[3]; 

  /* Add this speed to the extruder's position, with the coefficient */
  in.target[3] += es * linearAdvance_factor;

  return in;
}

