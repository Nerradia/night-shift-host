
#include <instructionWait.hpp>

#include <host.hpp>
#include <printer.hpp>
#include <thread>
#include <chrono>

/* Mapping functions */
instructionDefinition InstructionWait::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionWait::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionWait::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("G4");

  return list;
}

Instruction* InstructionWait::mapHook(std::string gcode) {
  return new InstructionWait(gcode);
}

InstructionWait::InstructionWait(std::string gcode) {
  
  auto gcodeData = instructionUtils::parseArguments(gcode);

  /* Parse P argument */
  if (gcodeData.find("P") != gcodeData.end()) {
    time_millis = gcodeData["P"];
  }

  this->time_millis = time_millis;
}

InstructionWait::~InstructionWait() {

}

std::string InstructionWait::print() {
  std::string s;
  s += "Wait " + std::to_string(time_millis) + " ms";
  return s;
}

/* TODO: Make it synchronous to the Motion pipeline */
void InstructionWait::run() {
  host.waitMotionPipelineEmpty();
  std::this_thread::sleep_for(std::chrono::milliseconds((int)time_millis));
}
