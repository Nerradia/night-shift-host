
#include <instructionSetPosition.hpp>
#include <instructionUtils.hpp>
#include <interpolation.hpp>
#include <host.hpp>

/* Mapping functions */
instructionDefinition InstructionSetPosition::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionSetPosition::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionSetPosition::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("G92");

  return list;
}

Instruction* InstructionSetPosition::mapHook(std::string gcode) {
  return new InstructionSetPosition(gcode);
}



InstructionSetPosition::InstructionSetPosition(std::string gcode) {

  auto gcodeData = instructionUtils::parseArguments(gcode);

  /* Check what axis is mentionned */
  for (int i = 0; i < Config::n_axes; i++) {
    std::string axisName = Config::axisToGcodeMap.at(i);

    if (gcodeData.find(axisName) != gcodeData.end()) {
      data.push_back({i, instructionUtils::gcodeLengthToStandard(gcodeData[axisName])});
    }
  }
}

InstructionSetPosition::~InstructionSetPosition() {

}

std::string InstructionSetPosition::print() {
  std::string s;
  s += "Set axis ";

  for (const auto &d : data) {
    s += Config::axisToGcodeMap[d.axis];
    s += ":";
    s += d.position;
    s += " ";
  }

  return s;
}

void InstructionSetPosition::run() {
  for (const auto &d : data) {
    host.setCurrentPosition(d.axis, d.position);
  }
}
