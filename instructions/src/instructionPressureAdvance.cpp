
#include <instructionPressureAdvance.hpp>

#include <host.hpp>
#include <printer.hpp>
#include <thread>
#include <chrono>

/* Mapping functions */
instructionDefinition instructionPressureAdvance::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = instructionPressureAdvance::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> instructionPressureAdvance::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("M900");

  return list;
}

Instruction* instructionPressureAdvance::mapHook(std::string gcode) {
  return new instructionPressureAdvance(gcode);
}

instructionPressureAdvance::instructionPressureAdvance(std::string gcode) {
  
  auto gcodeData = instructionUtils::parseArguments(gcode);

  /* Parse P argument */
  if (gcodeData.find("K") != gcodeData.end()) {
    pressureAdvanceCoef = gcodeData["K"];
  }

  this->pressureAdvanceCoef = pressureAdvanceCoef;
}

instructionPressureAdvance::~instructionPressureAdvance() {

}

std::string instructionPressureAdvance::print() {
  std::string s;
  s += "Set pressure advance coefficient to " + std::to_string(pressureAdvanceCoef) + " s^(-1)";
  return s;
}

/* TODO: Make it synchronous to the Motion pipeline */
void instructionPressureAdvance::run() {
  host.setPressureAdvance(pressureAdvanceCoef);
}
