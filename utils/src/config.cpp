
#include <config.hpp>

std::map<int, std::string> Config::axisToGcodeMap = {
  {0, "X"},
  {1, "Y"},
  {2, "Z"},
  {3, "E"}
};

std::array<std::vector<int>, Config::n_axes> Config::axisToStepper;
std::array<int, Config::n_axes> Config::axisHomingEndstop;
std::array<int, Config::n_axes> Config::axisHomingChannel;
std::array<double, Config::n_motors> Config::stepperResolution;
std::array<double, Config::n_axes> Config::probing_point;


std::array<double, Config::n_axes> Config::axisMaxSpeed;
std::array<double, Config::n_axes> Config::axisMaxAcceleration;

std::array<double, Config::n_axes> Config::homing_position;
std::vector<double> Config::homing_direction;

std::vector<int> Config::homing_axes;
std::array<int, Config::n_axes> Config::absolute_axes;

std::array<double, Config::n_axes> Config::nozzle_switch_pos;

Config::Config() {
  /* X */
  axisToStepper[0] = {1, 2};
  
  /* Y */
  axisToStepper[1] = {3, 4};

  /* Z */
  axisToStepper[2] = {5, 6, 8, 9};

  /* Extruder 0 */
  axisToStepper[3] = {7, 0};


   /* Endstops for origin */
  axisHomingEndstop[0] = 10; // X
  axisHomingEndstop[1] = 11; // Y
  axisHomingEndstop[2] = 0;  // Z - BLTouch

  /* Axis to FPGA channel mapping */
  // TODO: make it automatic and dynamic
  axisHomingChannel[0] = 1;
  axisHomingChannel[1] = 2;
  axisHomingChannel[2] = 3;

  homing_axes = {0, 1};  // X and Y are allowed to home

  absolute_axes = {0, 1, 2}; // X, Y and Z position is absolute and won't overflow, they can be resynchronized witht the FPGA's counters

  homing_position = {0, 0, 0, 0};
  homing_direction = {-1, -1, -1};

  probing_point[0] = 0.265;
  probing_point[1] = 0.16;
  probing_point[2] = bltouch_offset;
  probing_point[3] = 0;

  stepperResolution.fill(0.);
  /* Meters per steps, sign defines direction */
  stepperResolution[1] = -(0.040/200/256); // 20 GT2 teeh (20 * 2mm), 200 steps per revolution, 256-microstepping
  stepperResolution[2] = +(0.040/200/256);
  stepperResolution[3] = -(0.040/200/256);
  stepperResolution[4] = +(0.040/200/256);

  stepperResolution[5] = -0.002*24/200/256 * 16/80;
  stepperResolution[6] = -0.002*24/200/256 * 16/80;
  stepperResolution[8] = +0.002*24/200/256 * 16/80;
  stepperResolution[9] = -0.002*24/200/256 * 16/80; // 15 GT2 teeth (15 * 2mm), 200 steps/rev, 256-microstepping, and belt reduction by 5

  /* Orbiter doc says 690 steps/mm @ 16 micro-stepping */
  stepperResolution[7] = 0.001/690./16.; // Invert data for resolution, divide by 16 because we use 256-microstepping
  
  
  stepperResolution[0] = 0.001/415./16.; // Invert data for resolution, divide by 16 because we use 256-microstepping


  axisMaxSpeed[0] = 0.5;
  axisMaxSpeed[1] = 0.5;
  axisMaxSpeed[2] = 0.05;
  axisMaxSpeed[3] = 0.5;

  axisMaxAcceleration[0] = 2;
  axisMaxAcceleration[1] = 2;
  axisMaxAcceleration[2] = 2;
  axisMaxAcceleration[3] = 1;

  
  nozzle_switch_pos[0] = 0.05;
  nozzle_switch_pos[1] = 0.05;
  nozzle_switch_pos[2] = 0.1;
  nozzle_switch_pos[3] = 0;
}


Config config;
