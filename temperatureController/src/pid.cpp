
#include <pid.hpp> 
#include <config.hpp>
 

PIDController::PIDController() {
  param_p = 0;
  param_i = 0;
  param_d = 0;
  old_error = 0;
  integral = 0;
}

PIDController::~PIDController() {

}


double PIDController::refresh(double input, double target) {

  double error = target - input;
  
  /* Derivate */
  double d = (error - old_error) * param_p;
  old_error = error;
  
  /* Proportionnal */
  double p = error * param_p;

  /* Integral */
  double i = 0;
  if (param_i != 0.) {
    integral += error;
    
    i = integral * param_i;

    if (i > 1 - p) {
      i = p;
      integral = i / param_i;
    } 
    else if (i < 0) {
      i = 0;
      integral = 0;
    } 
  }

  double output = p + i + d;

  if (output < 0) output = 0;
  if (output > 1) output = 1;

  return output;
}

void PIDController::setParameters(double p, double i, double d) {
  param_p = p;
  param_i = i;
  param_d = d;
}

void PIDController::reset() {
  integral = 0;
}
