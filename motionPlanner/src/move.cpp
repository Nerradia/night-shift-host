
#include <move.hpp>
#include <cmath>

Move::Move() {
  bezier = false;
}

double Move::length() {
  return relative().norm();
}

AxesVector Move::relative() {
  return target - origin;
}

double Move::relative(int i) {
  return target[i] - origin[i];
}

double Move::getTargetSpeed(int i) {
  return std::abs(relative(i)) / length() * targetSpeed;
}

double Move::getTargetTime() {
  return length() / targetSpeed;
}

AxesVector Move::getTargetSpeed() {
  return abs(relative()) / length() * targetSpeed;
}

AxesVector Move::getStartTangent() {
  if (bezier) {
    AxesVector diff = bezier_p1 - origin;
    return diff / diff.norm();
  } 
  else {
    return relative() / length();
  }
}

AxesVector Move::getEndTangent() {
  if (bezier) {
    AxesVector diff = target - bezier_p2;
    return diff / diff.norm();
  } 
  else {
    return relative() / length();
  }
}

AxesVector Move::getNormalized() {
  return relative() / length();
}

AxesVector Move::getSample(double p) {

  if (p < 0 || p > 1) {
    throw std::string("Requested a sample with p out of bounds !");
  }

  if (bezier) {
    return  origin    * (std::pow(1 - p, 3) * std::pow(p, 0))
          + bezier_p1 * (std::pow(1 - p, 2) * std::pow(p, 1)) * 3
          + bezier_p2 * (std::pow(1 - p, 1) * std::pow(p, 2)) * 3
          + target    * (std::pow(1 - p, 0) * std::pow(p, 3));
  } 
  else {
    return  origin * (1 - p)
          + target * p;
  }

}

std::string Move::print() {
  std::string s;
  s = "Move Start: ";
  s += origin.print();
  s += " End: ";
  s += target.print();
  s += " speed: " + std::to_string(targetSpeed) + "; length : " + std::to_string(length());
  return s;
}
