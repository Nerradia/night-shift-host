#ifndef _THREAD__
#define _THREAD__

#include <stdint.h>
#include <mutex>
#include <thread>
#include <iostream>
#include <fstream>
#include <queue>

class thread {
public:
  
  enum E_status {
    E_STATUS_STOPPED,
    E_STATUS_RUNNING,
    E_STATUS_IDLE
  };
  
  thread();
  ~thread();
  
  enum E_status getStatus();
  std::string getStatusString();

  void start();
  void stop();

  virtual void process() = 0;

  std::thread *thread_handle; // TODO: put in private 
  
  /* Use this function to log things that could help get the context before an 
     error is thrown */
  void trace(std::string s);

protected:
  virtual std::string threadName()= 0;


  void setStatusString(std::string status);
  void setStatus(enum E_status);
  int stop_request;
  std::mutex m_status;
  enum E_status status;
  std::string status_string;
  void logstring(std::string s);

  /* std::cout and file stream mutex common to all threads */
  static std::mutex *coutmutex;

  std::queue<std::string> traceQueue;

  /* Common file stream output to all threads */
  static std::ofstream *traceFile;

  const unsigned int traceLogSize = 10;
};

extern bool aThreadCrashed;

#endif