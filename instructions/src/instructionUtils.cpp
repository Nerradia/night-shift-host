
#include <instructionUtils.hpp>
#include <cstring>

std::map<std::string, double> instructionUtils::parseArguments(std::string &gcode) {

  std::map<std::string, double> out;

  char gcode_cs[gcode.length() + 1];
  strcpy(gcode_cs, gcode.data());

  /* Go through each character */
  int i = 0;
  while(gcode_cs[i]) {
    /* Convert all chars to uppercase */
    char c = toupper(gcode_cs[i]);

    /* End parsing if it is a comment */
    if (c == ';') {
      break;
    }

    /* New key */
    if (isalpha(c)) {
      /* Parse */
      std::string key = std::string(1, c);
      double val = atof(&gcode_cs[i+1]);

      /* Add to output */
      out[key] = val;
    }
    
    i++;
  }


  return out;
}


double instructionUtils::gcodeLengthToStandard(double in) {
  //TODO: support inches if needed
  return in/1000.;
}

double instructionUtils::gcodeSpeedToStandard(double in) {
  //TODO: support inches if needed
  return in / 1000. / 60.;
}


