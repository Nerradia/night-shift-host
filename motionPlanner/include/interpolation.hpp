#ifndef INTERPOLATION
#define INTERPOLATION

#include <thread.hpp>
#include <move.hpp>
#include <safeQueue.hpp>
#include <safeVector.hpp>

class Interpolation : public thread {
  public:
  Interpolation(SafeQueue<Move>* inputQueue, SafeVector<Move>* accelQueue);
  ~Interpolation();
  void process();
  std::string threadName();

  void flush();

  private:
  /* Queue between host thread and the interpolation thread */
  SafeQueue<Move>* inputQueue;
  /* Queue shared between the interpolation thread and the acceleration planner
     thread. Vector instead of a queue because acceleration thread needs to see
     the whole data */
  SafeVector<Move>* accelQueue;
  bool flushRequest;

};

#endif /* INTERPOLATION */
