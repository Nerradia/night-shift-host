
#include <temperatureController.hpp>
#include <printer.hpp>

TemperatureController::TemperatureController() {
  pid[0].setParameters(0.02,
                       0.001,
                       0);
  pid[1].setParameters(0.2, 0.001, 0.00);

  temp_target[0] = 0;
  temp_target[1] = 0;
  
  temp_log.open("temp_log.csv");
}

TemperatureController::~TemperatureController() {

}

double TemperatureController::getTemp(int id) {
  return temp[id];
}

double TemperatureController::getTarget(int id) {
  return temp_target[id];
}

double TemperatureController::getPwr(int id) {
  return powerOut[id];
}

void TemperatureController::setTarget(int id, double temp) {
  //TODO: securize this
  temp_target[id] = temp;

  if (temp == 0) pid[id].reset();
}

void TemperatureController::process() {
  
  while(!stop_request) {

    status = E_STATUS_RUNNING;

    /* Get probe temperatures */
    temp[0] = printer.tempProbe[0].getTemp();
    temp[1] = printer.tempProbe[1].getTemp();

    for(int i = 0; i < Config::n_heaters; i++) {

      /* If heating enabled, compute PID */
      if (temp_target[i] != 0.) {
        //powerOut[i] = temp_target[i] / 100.;
        //if (powerOut[i] > 1.) powerOut[i] = 1.;
        powerOut[i] = pid[i].refresh(temp[i], temp_target[i]);
      } else {
        powerOut[i] = 0.;
      }
    }

    /* Set output */
    printer.setOutputPower(7, powerOut[0]);
    printer.setOutputPower(8, powerOut[1]);


    /* Log to file */
    for (int i = 0; i < Config::n_heaters; i++) {
      if (i) {
        temp_log << ", ";
      }   
      temp_log << temp[i] << ", " << temp_target[i] << ", " << powerOut[i];
    }
    temp_log << std::endl;

    status = E_STATUS_IDLE;
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }
}

std::string TemperatureController::threadName() {
  return "Temp controller";
}

TemperatureController temperatureController;
