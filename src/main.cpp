//Generic libs
#include <iostream>

#include <host.hpp>
#include <motionPlanner.hpp>
#include <printer.hpp>
#include <inputManager.hpp>
#include <temperatureController.hpp>
#include <wsInterface.hpp>
#include <motionOutput.hpp>

#include <thread>
#include <unistd.h>
#include <chrono>
#include <cmath>

#include <ftxui/dom/elements.hpp>
#include <ftxui/screen/screen.hpp>

int main(int argc, char** argv){

#if 0
  wsInterface.start();
#endif
#if 0
  std::vector<std::vector<double>> map = {{0.000, 0.001}, {0.000, 0.001}};

  motionPlanner.motionOutput->setBedLevelingOffsets(map, 2, 0.2, 0.2, 0, 0);

  AxesVector a;
  a[0] = 0.3;
  a[1] = 0.1;
  a[2] = 0;
  a[3] = 0;

  std::cout << a.print() << std::endl;
  std::cout << motionPlanner.motionOutput->bedLeveling(a).print() << std::endl;
#endif
#if 1
  if (argc != 2) {
    return 1;
  }

  inputManager.setFile(std::string(argv[1]));

  host.start();

  #if 1

  /* Terminal UI loop */
  using namespace ftxui;
  std::string reset_position;
  while(!aThreadCrashed) {
    try {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      
      int current_temp_update = printer.getCurrentAdcChannel();
      TriggerMask trig_state = printer.getTriggerState();

      Element document =
      hbox({
        /* vbox for all thread status */
        vbox({
          /* Input thread vbox */
          vbox(
            text("Input thread"),
            inputManager.getStatus() == thread::E_STATUS_RUNNING ? color(Color::Green, text("Running")) : color(Color::Yellow, text("IDLE")),
            text(inputManager.getStatusString())
          ) | border,

          /* Host Thread vbox */
          vbox(
            text("Host thread"),
            host.getStatus() == thread::E_STATUS_RUNNING ? color(Color::Green, text("Running")) : color(Color::Yellow, text("IDLE")),
            text(host.getStatusString())
          ) | border,

          /*Temperature thread vbox */
          vbox(
            text("temperatureController thread"),
            temperatureController.getStatus() == thread::E_STATUS_RUNNING ? color(Color::Green, text("Running")) : color(Color::Yellow, text("IDLE")),
            text(temperatureController.getStatusString())
          ) | border,

          /* Motion pipeline hbox */
          hbox({
            /* Interpolation */
            vbox(
              text("Interpolation thread"),
              motionPlanner.interpolation->getStatus() == thread::E_STATUS_RUNNING ? color(Color::Green, text("Running")) : color(Color::Yellow, text("IDLE")),
              text(motionPlanner.interpolation->getStatusString())
            ) | border,

          /* Motion */
            vbox(
              text("Motion thread"),
              motionPlanner.accelerationPlanner->getStatus() == thread::E_STATUS_RUNNING ? color(Color::Green, text("Running")) : color(Color::Yellow, text("IDLE")),
              text(motionPlanner.accelerationPlanner->getStatusString())
            ) | border,
            /* Output queue */
            vbox(
              text("Output thread"),
              motionPlanner.motionOutput->getStatus() == thread::E_STATUS_RUNNING ? color(Color::Green, text("Running")) : color(Color::Yellow, text("IDLE")),
              text(motionPlanner.motionOutput->getStatusString())
            ) | border,
          })
        }) | size(WIDTH, EQUAL, 100),
        vbox({
          /* Queues status */
          vbox(
            text("Queues"),
            hbox(
              vbox(
                text("host "),
                text("interpolation "),
                text("motionPlanner "),
                text("motionOutput "),
                text("FPGA ")
              ),
              vbox(
                gauge(host.getQueueSize()),
                gauge(motionPlanner.getQueueSize(0)),
                gauge(motionPlanner.getQueueSize(1)),
                gauge(motionPlanner.getQueueSize(2)),
                gauge(printer.getFifoSize())
              ) | size(WIDTH, EQUAL, 20)
            )
          )| border,
          /* Position status */
          vbox({
            vbox(
              text("Current position"),
              hbox(
                vbox(
                  text("Axis "),
                  text("X "),
                  text("Y "),
                  text("Z "),
                  text("E ")
                ),
                vbox(
                  text("Host pos "),
                  text(std::to_string(host.pos.current[0]) + " "),
                  text(std::to_string(host.pos.current[1]) + " "),
                  text(std::to_string(host.pos.current[2]) + " "),
                  text(std::to_string(host.pos.current[3]) + " ")
                ) | size(WIDTH, EQUAL, 20),
                vbox(
                  text("Host offset "),
                  text(std::to_string(motionPlanner.getHostOffset(0)) + " "),
                  text(std::to_string(motionPlanner.getHostOffset(1)) + " "),
                  text(std::to_string(motionPlanner.getHostOffset(2)) + " "),
                  text(std::to_string(motionPlanner.getHostOffset(3)) + " ")
                ) | size(WIDTH, EQUAL, 20),
                vbox(
                  text("MP "),
                  text(std::to_string(motionPlanner.getMPCurrentPosition(0)) + " "),
                  text(std::to_string(motionPlanner.getMPCurrentPosition(1)) + " "),
                  text(std::to_string(motionPlanner.getMPCurrentPosition(2)) + " "),
                  text(std::to_string(motionPlanner.getMPCurrentPosition(3)) + " ")
                ) | size(WIDTH, EQUAL, 20),
                vbox(
                  text("MP Out"),
                  text(std::to_string(motionPlanner.motionOutput->getPosition(0)) + " "),
                  text(std::to_string(motionPlanner.motionOutput->getPosition(1)) + " "),
                  text(std::to_string(motionPlanner.motionOutput->getPosition(2)) + " "),
                  text(std::to_string(motionPlanner.motionOutput->getPosition(3)) + " ")
                ),
                vbox(
                  text("FPGA pos "),
                  text(std::to_string(printer.getCurrentPos(1)) + " "),
                  text(std::to_string(printer.getCurrentPos(3)) + " "),
                  text(std::to_string(printer.getCurrentPos(5)) + " "),
                  text(std::to_string(printer.getCurrentPos(7)) + " ")
                ) | size(WIDTH, EQUAL, 20),
                vbox(
                  text("FPGA cap "),
                  text(std::to_string(printer.getCapturedPos(1)) + " "),
                  text(std::to_string(printer.getCapturedPos(3)) + " "),
                  text(std::to_string(printer.getCapturedPos(5)) + " "),
                  text(std::to_string(printer.getCapturedPos(7)) + " ")
                ) | size(WIDTH, EQUAL, 20)
              )
            )| border,
          }),
          /* Triggers status */
          vbox({
            vbox(
              text("Triggers"),
              hbox(
                vbox(
                  text("ID "),
                  text("0 "),
                  text("1 "),
                  text("2 "),
                  text("3 "),
                  text("4 "),
                  text("5 "),
                  text("6 "),
                  text("7 ")
                ),
                vbox(
                  text("Input "),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[0]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[1]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[2]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[3]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[4]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[5]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[6]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.trigger_in_sel[7])))
                ),
                vbox(
                  text("Output "),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[0]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[1]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[2]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[3]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[4]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[5]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[6]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_sel[7])))
                ),
                vbox(
                  text("Action "),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[0]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[1]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[2]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[3]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[4]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[5]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[6]))),
                  text(std::to_string(printer.fpga.read(printer.fpga.position_controller.drv_action[7])))
                ),
                vbox(
                  text("State "),
                  text(trig_state.getBit(0) ? "T" : " "),
                  text(trig_state.getBit(1) ? "T" : " "),
                  text(trig_state.getBit(2) ? "T" : " "),
                  text(trig_state.getBit(3) ? "T" : " "),
                  text(trig_state.getBit(4) ? "T" : " "),
                  text(trig_state.getBit(5) ? "T" : " "),
                  text(trig_state.getBit(6) ? "T" : " "),
                  text(trig_state.getBit(7) ? "T" : " ")
                )
              )
            )| border
          })
        }),
        /* Temperatures status */
        vbox({
          vbox(
            text("Temperatures"),
            hbox(
              vbox(
                text("  "),
                text(current_temp_update == 0 ? ">" : " "),
                text(current_temp_update == 1 ? ">" : " "),
                text(current_temp_update == 2 ? ">" : " "),
                text(current_temp_update == 3 ? ">" : " "),
                text(current_temp_update == 4 ? ">" : " "),
                text(current_temp_update == 5 ? ">" : " "),
                text(current_temp_update == 6 ? ">" : " ")
              ),
              vbox(
                text("     "),
                text(printer.tempProbe[0].getName()),
                text(printer.tempProbe[1].getName()),
                text(printer.tempProbe[2].getName()),
                text(printer.tempProbe[3].getName()),
                text(printer.tempProbe[4].getName()),
                text(printer.tempProbe[5].getName()),
                text(printer.tempProbe[6].getName())
              ),
              vbox(
                text("Current "),
                text(std::to_string((int)std::round(printer.tempProbe[0].getTemp())) + "°C "),
                text(std::to_string((int)std::round(printer.tempProbe[1].getTemp())) + "°C "),
                text(std::to_string((int)std::round(printer.tempProbe[2].getTemp())) + "°C "),
                text(std::to_string((int)std::round(printer.tempProbe[3].getTemp())) + "°C "),
                text(std::to_string((int)std::round(printer.tempProbe[4].getTemp())) + "°C "),
                text(std::to_string((int)std::round(printer.tempProbe[5].getTemp())) + "°C "),
                text(std::to_string((int)std::round(printer.tempProbe[6].getTemp())) + "°C ")
              ),
              vbox(
                text("Target "),
                text(std::to_string((int)std::round(temperatureController.getTarget(0))) + "°C "),
                text(std::to_string((int)std::round(temperatureController.getTarget(1))) + "°C ")
              ),
              vbox(
                text("Power "),
                text(std::to_string((int)std::round(temperatureController.getPwr(0) * 100.)) + " %"),
                text(std::to_string((int)std::round(temperatureController.getPwr(1) * 100.)) + " %")
              )
            )
          )| border ,
          text("Pressure advance : " + std::to_string(motionPlanner.motionOutput->linearAdvance_factor))
        })
      });

      auto screen = Screen::Create(
        Dimension::Full(),       // Width
        Dimension::Full() // Height
      );
      Render(screen, document);
      if (!aThreadCrashed) {
        std::cout << reset_position;
        screen.Print();
      }
      reset_position = screen.ResetPosition();
    }
    catch (...) {
      std::cout << "Error rendering console !" << std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  }

  host.thread_handle->join();

  #else 
  while(1) {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }
  #endif

#endif

  wsInterface.thread_handle->join();

	return 0;

}

