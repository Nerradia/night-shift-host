#ifndef TEMPPROBE
#define TEMPPROBE

#include <string>

class TempProbe {
public:
  TempProbe();
  ~TempProbe();

  void configure(int adc_channel, double Rup, double Rdown, double r0, double t0, double B, std::string name);

  std::string getName();
  double getTemp();

private:
  std::string name;
  int adc_channel;
  double Rup_eq;
  double Vup_eq;
  double Rdown;
  double r0;
  double t0;
  double B;

};

#endif /* TEMPPROBE */
