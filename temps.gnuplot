
set term pngcairo
set output "temp_log.png"
set ytics nomirror
set datafile separator ','
set y2tics -0.1, 0.1
set y2range [-0.1:1.1]
set yrange [-0:250]
set ytics 0, 10
plot "temp_log.csv" using 1 with lines  axis x1y1, '' using 2 with lines  axis x1y1, '' using 3 with lines axis x1y2

