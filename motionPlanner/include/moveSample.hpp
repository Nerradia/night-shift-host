#ifndef MOVESAMPLE
#define MOVESAMPLE

#include <config.hpp>
#include <axesVector.hpp>

class MoveSample {
 public:
  MoveSample();
  MoveSample(AxesVector target, double speed);

  /* Where to go */
  AxesVector target;
  /* What speed */
  double speed;
  
};

#endif /* MOVESAMPLE */
