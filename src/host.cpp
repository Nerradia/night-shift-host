
#include <host.hpp>
#include <config.hpp>
#include <interpolation.hpp>
#include <inputManager.hpp>
#include <motionPlanner.hpp>
#include <temperatureController.hpp>
#include <printer.hpp>
#include <motionOutput.hpp>

Host::Host() {
  hoststate = INIT;

  for (int i = 0; i < Config::n_axes; i++) {
    pos.current[i] = 0;
  }

  pos.speed = Config::default_speed;
  pos.mode = host.POS_ABSOLUTE;

}

Host::~Host() {
  
}



void Host::process() {

  while(!stop_request) {
    
    switch (hoststate) {
      case INIT:
        processInit();
        break;

      case RUNNING:
        processRun();
        break;

      case STOP:
        processStop();
        break;

      default:
        throw std::string("Unknown Host state !");
    }
  }
}

/* Called in a loop at software startup */
void Host::processInit() {

  printer.init();
  
  trace("FPGA Version : " + printer.getFPGAVersion());
  trace("FPGA Build : " + printer.getFPGABuild());

  TriggerMask endstopPolarity;
  endstopPolarity.setAll();
  endstopPolarity.resetBit(0);
  printer.setEndstopPolarity(endstopPolarity);

  /* Use trigger 0 for X (endstop 14) */
  //printer.setTriggerInput(0, 14);
  host.freeEndstop(0);
  host.freeEndstop(1);
  host.freeEndstop(2);
  host.resetEndstopState(0);
  host.resetEndstopState(1);
  host.resetEndstopState(2);
  printer.resetTriggers();
  
  printer.initDrivers();

  /* Extruder switching */
  printer.setOutputFreq(4, 500); // No PWM anyway
  setCurrentExtruder(0);
  
  printer.enableDriver(0); //  E0
  printer.enableDriver(1); //  X
  printer.enableDriver(2); // -X
  printer.enableDriver(3); //  Y
  printer.enableDriver(4); // -Y
  printer.enableDriver(5); //  Z
  printer.enableDriver(6); //  Z
  printer.enableDriver(8); //  Z
  printer.enableDriver(9); //  Z
  printer.enableDriver(7); //  E

  /* Fans */
  printer.setOutputFreq(5, 10);
  printer.setOutputFreq(6, 10);

  printer.setOutputPower(5, 1);
  printer.setOutputPower(6, 0);
  
  /* Light */
  printer.setOutputFreq(0, 500);
  printer.setOutputPower(0, 1.);


  /* Power outputs */
  printer.setOutputFreq(7, Config::f_heater0);
  printer.setOutputFreq(8, Config::f_heater1);

  /* Disable watchdog for fans, light and extruder switching */
  printer.setOutputWatchdogMask((1 << 0) | (1 << 4) | (1 << 5) | (1 << 6));

  printer.setTempPriorityMask((1 << 0) | (1 << 1));
  /* Init temperature probes with parameters */
  printer.tempProbe[0].configure(0, 4.7e3, 4.7e3, 100e3, 25, 4267, "E1");
  printer.tempProbe[1].configure(1, 4.7e3, 4.7e3, 100e3, 25, 3950, "Bed");
  printer.tempProbe[2].configure(8, 4.7e3, 4.7e3, 100e3, 25, 4700, "Drv1");
  printer.tempProbe[3].configure(9, 4.7e3, 4.7e3, 100e3, 25, 4700, "Drv2");
  printer.tempProbe[4].configure(10, 4.7e3, 4.7e3, 100e3, 25, 4700, "MOS1");
  printer.tempProbe[5].configure(11, 4.7e3, 4.7e3, 100e3, 25, 4700, "MOS2");
  printer.tempProbe[6].configure(12, 4.7e3, 4.7e3, 100e3, 25, 4700, "PSU");

  temperatureController.start();
  inputManager.start();

  motionPlanner.start();

  /* Reset FPGA's position counters */
  printer.resetCurrentPos();

  /* Init done, switch to RUNNING state to accept inputs */
  hoststate = RUNNING;
}

/* Called in a loop while the printer should stop and host should exit */
void Host::processStop() {

  inputManager.thread_handle->join();
  motionPlanner.stop();

  printer.setOutput(1, 0., 1);
  printer.disableDriver(1); //  X
  printer.disableDriver(2); // -X
  printer.disableDriver(3); //  Y
  printer.disableDriver(4); // -Y
  printer.disableDriver(5); //  Z
  printer.disableDriver(7); //  E
}

/* Called in a loop while in RUNNING state */
void Host::processRun() {

  if (instructionQueue.empty()) {
    status = E_STATUS_IDLE;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

  } else {
    status = E_STATUS_RUNNING;
    Instruction* i;
    i = instructionQueue.front();
    status_string = "Running " + i->print();
    trace(status_string);
    i->run();
    instructionQueue.pop();
    delete i;
  }
}

void Host::queueInstruction(Instruction* i) {

  while(instructionQueue.size() > Config::instructionQueueSize) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  
  instructionQueue.push(i);

}

double Host::getQueueSize() {
  return (double)instructionQueue.size() / (double)Config::instructionQueueSize;
}

void Host::flushMotionPipeline() {
  trace("flushMotionPipeline");
  motionPlanner.flush();
}

void Host::syncMotionPipeline() {
  trace("syncMotionPipeline");
  motionPlanner.resync();
  trace("MotionPlanner position is now " + motionPlanner.getMPCurrentPosition().print());
}

void Host::waitMotionPipelineEmpty() {
  trace("waitMotionPipelineEmpty");
  motionPlanner.waitIDLE();
}

std::string Host::threadName() {
  return "host";
}

std::array<double, Config::n_axes> Host::getAbsolutePosition() {
  return pos.current;
}

double Host::getCapturedPosition(int axis) {
  return motionPlanner.getCapturedPosition(axis);
}

void Host::moveAbsolute(std::array<double, Config::n_axes> target, double speed) {
  pos.current = target;
  pos.speed = speed;
  
  move();
}

void Host::moveAbsolute(std::array<double, Config::n_axes> target) {
  pos.current = target;
  
  move();
}

void Host::moveRelative(std::array<double, Config::n_axes> target, double speed) {
  
  for (int i = 0; i < Config::n_axes; i++) {
    pos.current[i] += target[i];
  }

  pos.speed = speed;

  move();
}

void Host::moveRelative(std::array<double, Config::n_axes> target) {
  
  for (int i = 0; i < Config::n_axes; i++) {
    pos.current[i] += target[i];
  }

  move();
}

  
/* Prepare a move setting targets axis per axis then run it */
void Host::setTargetAbsolute(int axis, double target) {
  pos.current[axis] = target;
}

double Host::getTargetAbsolute(int axis) {
  return pos.current[axis];
}

void Host::setTargetRelative(int axis, double target) {
  pos.current[axis] += target;
}

void Host::setSpeed(double speed) {
  pos.speed = speed;
}

void Host::goToTarget() {
  move();
}

void Host::setCurrentPosition(int axis, double currentPosition) {
  trace("setCurrentPosition(" + std::to_string(axis) + ", " + std::to_string(currentPosition) + ")");
  pos.current[axis] = currentPosition;
  motionPlanner.setCurrentPosition(pos.current);
  trace("HostOffset is now " + std::to_string(motionPlanner.getHostOffset(axis)));
}

void Host::setCurrentPosition(std::array<double, Config::n_axes> currentPosition) {
  for(int i = 0; i < Config::n_axes; i++) {
    setCurrentPosition(i, currentPosition[i]);
  }
}

void Host::setCurrentExtruder(int id) {
  // TODO: Use config instead of magic values
  trace("Switching to extruder " + std::to_string(id));

  if (id < 0 || id > 1) {
    throw std::string("Bad extruder ID given !");
  }

  if (id != currentExtruderId) {

    /* Disable the multiplexed extruder driver */
    printer.disableDriver(0);

    /* Wait a bit */
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    /* Switch stepper */
    switch (id) {
      case 0:
        printer.setOutputPower(4, 0.);
        break;

      case 1:
        printer.setOutputPower(4, 1.);
        break;

      default:
        throw std::string("Bad extruder ID given !");
    }

    /* Wait a bit */
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    /* Enable the multiplexed extruder driver */
    printer.enableDriver(0);

    /* Wait a bit */
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    currentExtruderId = id;
  }
}

void Host::setPressureAdvance(double coef) {
  
  // TODO: rename functions so they all talk about the same thing
  motionPlanner.setLinearAdvance(coef);
}

void Host::move() {
  
  std::array<double, Config::n_axes> target = host.pos.current;
  
  trace("Queueing move " + AxesVector(target).print() + " with speed " + std::to_string(pos.speed));
  motionPlanner.queueMove(target, pos.speed);

}

void Host::setEndstop(int axis) {
  
  /* Link endstop to FPGA's channel */
  printer.setTriggerInput(Config::axisHomingChannel[axis], Config::axisHomingEndstop[axis]);

  for (auto &&stepper : Config::axisToStepper[axis]) {
    /* Set channel for each motor of this axis */
    printer.setMotorTrigger(stepper, Config::axisHomingChannel[axis]);

    /* Set actions for each motor : don't capture, stop motor and reset FPGA's position counter */
    printer.setMotorActions(stepper, false, true, true);
  }
}

void Host::setProbe(int axis) {
  
  /* Link endstop to FPGA's channel */
  printer.setTriggerInput(Config::axisHomingChannel[axis], Config::axisHomingEndstop[axis]);

  for (auto &&stepper : Config::axisToStepper[axis]) {
    /* Set channel for each motor of this axis */
    printer.setMotorTrigger(stepper, Config::axisHomingChannel[axis]);

    /* Set actions for each motor : capture position and stop motor */
    printer.setMotorActions(stepper, true, false, true);
  }
}

void Host::resetEndstopState(int axis) {
  printer.resetTrigger(Config::axisHomingChannel[axis]);
}

bool Host::getEndstopState(int axis) {
  
  TriggerMask trig_state;
  trig_state = printer.getTriggerState();
  
  return trig_state.getBit(Config::axisHomingChannel[axis]);
}

void Host::freeEndstop(int axis) {
  //TODO: Add an inactive endstop option in the FPGA
  for (auto &&stepper : Config::axisToStepper[axis]) {
    /* Set actions for each motor : don't capture, stop motor and reset FPGA's position counter */
    printer.setMotorActions(stepper, false, false, false);
  }
}

void Host::armBLtouch() {
  printer.blTouch.reset();
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  printer.blTouch.probe();
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}

void Host::disarmBLtouch() {
    /* Retract the BLTouch */
    printer.blTouch.retract();
}


void Host::setStepperOffset(int stepperId, double offset) {
  trace("setStepperOffset of stepper " + std::to_string(stepperId) + " : " + std::to_string(offset));
  motionPlanner.setStepperOffset(stepperId, offset);
}

void Host::setBedLevelingOffsets(std::vector<std::vector<double>> map, int np, double tileSizeX, double tileSizeY, double offsetX, double offsetY) {
  motionPlanner.setBedLevelingOffsets(map, np, tileSizeX, tileSizeY, offsetX, offsetY);
}


void Host::traceStr(std::string s){
  trace(s);
}

Host host;
