#ifndef INSTRUCTION_HPP__
#define INSTRUCTION_HPP__

#include <config.hpp>
#include <vector>
#include <string>
#include <functional>
#include <instructionUtils.hpp>

/* Generic class used for all instructions given from host's inputs */
class Instruction {
  public:
  virtual ~Instruction() = 0;

  virtual void run() = 0;
  virtual std::string print() = 0;

  /* Each child class should also implement this fuction */
  //static instructionDefinition getInstructionDefinition();

};


class instructionDefinition {
  public:
  std::vector<std::string> gcodeTokens;
  std::function<Instruction*(std::string)> constructor;
};

#endif
