#ifndef MOTIONPLANNER
#define MOTIONPLANNER

#include <thread.hpp>
#include <mutex>
#include <array>

#include <config.hpp>
#include <motionOutput.hpp>
#include <interpolation.hpp>
#include <accelerationPlanner.hpp>
#include <move.hpp>
#include <safeQueue.hpp>
#include <safeVector.hpp>

class MotionPlanner {

  public:
  MotionPlanner();
  ~MotionPlanner();

  void start();
  void stop();

  void waitIDLE();

  /* Queue a move in the host's reference */
  void queueMove(AxesVector target, double speed);

  /* Get the printer's position in host's reference */
  AxesVector getCurrentPosition();

  /* Set the current host's position */
  void setCurrentPosition(AxesVector position);

  /* Cancel everything that is queued. resync() is needed after that */
  void flush();
  
  /* After a flush, the motion pipeline must be resynchronized because the link
     between its input position and FPGA's position counter are not true anymore */
  void resync();

  /* Get the offset between host and motion planner for debugging */
  double getHostOffset(int axis);

   /* Get the captured position in host's reference */
   double getCapturedPosition(int axis);

  /* Get the position in the motion planner's reference */
   double getMPCurrentPosition(int axis);
   AxesVector getMPCurrentPosition();

   /* Get the queues current occupancy for debugging */
   double getQueueSize(int step);

   /* Set an offset to a stepper that will be applied on the next move,
     it should be a slow move, this offset is applied after acceleration planning ! */
   void setStepperOffset(int stepperId, double offset);
  
   /* Set the bed leveling offset map */
   void setBedLevelingOffsets(std::vector<std::vector<double>> map, int np, double tileSizeX, double tileSizeY, double offsetX, double offsetY);

   /* Set the linear advance factor */
   void setLinearAdvance(double factor);

  /* Threads */
  Interpolation *interpolation;
  AccelerationPlanner *accelerationPlanner;
  MotionOutput *motionOutput;

  private:
  /* Current position (at the input of the pipeline) */
  AxesVector currentPosition;

  /* Offset between the host's (the user) position and the position in the
     pipeline. */
  AxesVector hostOffset;

  /* Queue between host thread and the interpolation thread */
  SafeQueue<Move> inputQueue;
  /* Queue shared between the interpolation thread and the acceleration planner
     thread. Vector instead of a queue because acceleration thread needs to see
     the whole data */
  SafeVector<Move> accelQueue;
  /* Queue between the acceleration planner and the motion output thread */
  SafeQueue<Move> outputQueue;

  /* When host checks motionplanner is idle, the host thread should check the queues,
     instead of only relying on MP's threads to report their status.
     If host adds a movement and directly checks for idle, MP's threads could still be 
     in idle state because they did not have time to be run and fetch the queues */
   bool allQueuesEmpty();
};

extern MotionPlanner motionPlanner;

#endif /* MOTIONPLANNER */
