LIB = ../libs/temperatureController.a

GENINCLUDE = ../include
UTILSINCLUDE = ../utils/include

SRCDIR   = src
INCLDIR  = include
OBJDIR   = ../obj
BINDIR   = bin

MANUAL_TEST_FILE = tests/main.cpp
MANUAL_TEST_OUTPUT_FILE = temperatureControllertest

SOURCES  := $(wildcard $(SRCDIR)/*.cpp)
INCLUDES := $(wildcard $(INCLDIR)/*.hpp)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
DEPENDS  := $(OBJECTS:.o=.d)


CC = g++
CFLAGS = -lpthread  -Wall -std=c++11 -I$(INCLDIR) -I$(GENINCLUDE) -I$(UTILSINCLUDE) $(INC_SRCH_PATH)

$(LIB): $(OBJECTS)
	@mkdir -p ../libs
	$(AR) rvs $@ $^
	@echo "    Archive    $(notdir $@)"


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CC) -c $^ $(CFLAGS) -o $@

.PHONY: clean
clean:
	@$(RM) -f $(LIB) $(OBJECTS)
	@$(RM) -f *.expand
	@echo "    Remove Objects:   $(OBJECTS)"

#To test the library with the tests/main.cpp file, please use make -f temperatureController.mk test_m
.PHONY: test_m
test_m:
	$(CC) $(CFLAGS) $(MANUAL_TEST_FILE) -o $(MANUAL_TEST_OUTPUT_FILE)
