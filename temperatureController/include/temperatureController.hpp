#ifndef TEMPERATURECONTROLLER
#define TEMPERATURECONTROLLER

#include <thread.hpp>
#include <string>
#include <pid.hpp>
#include <config.hpp>
#include <fstream>

class TemperatureController : public thread {
public:
  TemperatureController();
  ~TemperatureController();

  void setTarget(int id, double temp);

  double getTemp(int id);
  double getTarget(int id);
  double getPwr(int id);

  void process();
  std::string threadName();

private:
  PIDController pid[Config::n_heaters];

  std::ofstream temp_log;
  
  double temp_target[Config::n_heaters];
  double temp[Config::n_heaters];
  double powerOut[Config::n_heaters];
  

};


extern TemperatureController temperatureController;


#endif /* TEMPERATURECONTROLLER */
