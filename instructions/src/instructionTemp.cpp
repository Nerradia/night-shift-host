
#include <instructionTemp.hpp>
#include <temperatureController.hpp>
#include <host.hpp>
#include <printer.hpp>
#include <cmath>


/* Mapping functions */
instructionDefinition InstructionTemp::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionTemp::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionTemp::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("M104");
  list.push_back("M109");
  list.push_back("M140");
  list.push_back("M190");

  return list;
}

Instruction* InstructionTemp::mapHook(std::string gcode) {
  return new InstructionTemp(gcode);
}

/* TODO: Handle the heater ID stuff with a map or something in Config:: */
InstructionTemp::InstructionTemp(std::string gcode) {

  auto gcodeData = instructionUtils::parseArguments(gcode);

  /* Check what gcode instruction was given */
  int gcodeNumber = std::round(gcodeData["M"]);
  switch(gcodeNumber) {
    case 104:
      is_blocking = false;
      heater_id = 0;
      break;

    case 109:
      is_blocking = true;
      heater_id = 0;
      break;

    case 140:
      is_blocking = false; 
      heater_id = 1;
      break;

    case 190:
      is_blocking = true;
      heater_id = 1;
      break;

    default:
      throw std::string("Unknown temperature argument !");
  }


  /* Parse temperature argument */
  if (gcodeData.find("S") != gcodeData.end()) {
    temp_target = gcodeData["S"];
  }
  
  host.traceStr("M" + std::to_string(gcodeNumber) + " Heater ID " + std::to_string(heater_id) + " set to " + std::to_string(temp_target)); 
}

InstructionTemp::~InstructionTemp() {

}

std::string InstructionTemp::print() {
  std::string s;
  s += "Set temperature of extruder " + std::to_string(1) + " to " + std::to_string(temp_target) + "°C";
  if (is_blocking) {
    s += " and wait";
  }
  return s;
}

void InstructionTemp::run() {

  temperatureController.setTarget(heater_id, temp_target);

  if (is_blocking) {
    /* Block while we are not in the +/- 3° range */
    while(std::abs(temp_target - temperatureController.getTemp(heater_id)) > 3) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  }

}
