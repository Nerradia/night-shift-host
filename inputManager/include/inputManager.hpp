#ifndef _INPUTMANAGER__
#define _INPUTMANAGER__

#include <thread.hpp>
#include <iostream>
#include <gcodeParser.hpp>
#include <serialInput.hpp>

class InputManager : public thread {
public:
    std::string threadName();
    InputManager(/* args */);
    ~InputManager();
    void process();
    
    // TODO: make a real input parser
    void setFile(std::string input_file_path);
    std::string filepath;
    int filepath_valid;

    gcodeParser gcodeparser;
};

extern InputManager inputManager; 

#endif