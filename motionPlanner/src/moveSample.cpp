
#include <moveSample.hpp>

MoveSample::MoveSample() {
  target.fill(0);
  speed = 0;
}

MoveSample::MoveSample(AxesVector target, double speed) {
  this->target = target;
  this->speed = speed;
}
