#ifndef INSTRUCTIONHOME
#define INSTRUCTIONHOME

#include <instruction.hpp>
#include <string>

class InstructionHome : public Instruction {
  public:
  InstructionHome(std::string gcode);
  ~InstructionHome();

  /* Gcode mapping stuff */
  static instructionDefinition getInstructionDefinition();
  static std::vector<std::string> getGcodeTokens();
  static Instruction* mapHook(std::string gcode);
  
  void run();
  std::string print();

  private:
  std::array<bool, Config::n_axes> homing_mask;

};

#endif /* INSTRUCTIONHOME */
