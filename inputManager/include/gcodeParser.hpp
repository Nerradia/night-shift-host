#ifndef GCODEPARSER
#define GCODEPARSER

#include <instruction.hpp>
#include <string.h>
#include <map>
#include <vector>
#include <iostream> //pour le debug
#include <functional>

class gcodeParser {
public:
  gcodeParser();
  ~gcodeParser();
    
  Instruction* parse(std::string gcode);

private:
  /* A map of all gcode instructions and what function should be called to 
     create the corresponding Instruction child */
  std::map<std::string, std::function<Instruction*(std::string)>> instructionMap;
};



#endif /* GCODEPARSER */
