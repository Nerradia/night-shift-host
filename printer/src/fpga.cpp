
#include "fpga.hpp"
#include <stdio.h>
#include <fstream>
#include <string.h>
#include "spi.hpp"

int FPGA::init(const char* spi_dev) {
  /* Init SPI */
  spi.init(spi_dev, 0, 2000000);

  return 0;
}

void FPGA::writeQueued(FPGA_Reg reg, uint32_t data) {

  uint16_t address = reg.addr_offset;

  /* SPI transaction buffer, filled with command, address and data */
  char buffer[7] = {WRITE_QUEUED,
                      (char) (address >> 8),
                      (char) address,
                      (char) (data >> 24),
                      (char) (data >> 16),
                      (char) (data >> 8),
                      (char) (data)};

  spi.readWrite(buffer, buffer, 7);
}

void FPGA::write(FPGA_Reg reg, uint32_t data) {

  uint16_t address = reg.addr_offset;

  /* SPI transaction buffer, filled with command, address and data */
  char buffer[7] = {WRITE_IMMEDIATE,
                      (char) (address >> 8),
                      (char) address,
                      (char) (data >> 24),
                      (char) (data >> 16),
                      (char) (data >> 8),
                      (char) (data)};

  spi.readWrite(buffer, buffer, 7);
}

uint32_t FPGA::read(FPGA_Reg reg) {

  uint16_t address = reg.addr_offset;

  /* SPI transaction buffer, filled with command and address */
  char buffer[9] = {READ_IMMEDIATE,
                      (char) (address >> 8),
                      (char) address,
                      0x00
                      };


  spi.readWrite(buffer, buffer, 9);

  return (buffer[5] << 24 | buffer[6] << 16 | buffer[7] << 8 | buffer[8]);
}

std::string FPGA::read(FPGA_Reg_string reg) {
  std::string out;

  for (int word = 0; word < reg.length; word++) {
    uint16_t address = reg.addr_offset + word;

    /* SPI transaction buffer, filled with command and address */
    char buffer[9] = {READ_IMMEDIATE,
                        (char) (address >> 8),
                        (char) address,
                        0x00
                        };

    spi.readWrite(buffer, buffer, 9);

    out.append(&buffer[5]);
  }

  return out;
}


void memcpyswap(char* out, char* input, int size) {
  for(int i = 0; i < size; i++) out[size-i-1] = input[i];
}

// TODO: Clean this magic constants mess
void FPGA::moveQueued(uint32_t clk_div, uint32_t max_steps, int32_t* steps, std::ofstream &f) {

  char buffer[1 + 2 * 4 + n_drivers * 4];
// char dbg_buffer[1024];
// char dbg_buffer2[8];

  /* command */
  buffer[0] = STEPS_QUEUED;

  /* clock divider */
  memcpyswap(buffer+1, (char*)&clk_div, sizeof(clk_div));
  
  /* maximum step value */
  memcpyswap(buffer+5, (char*)&max_steps, sizeof(max_steps));

  /* step values */
  for (int i = 0; i < n_drivers; i++) {
    memcpyswap(buffer + 9 + i * 4, (char*)&steps[n_drivers-i-1], 4);
  }

 // f << "\r\nWrite buffer to FPGA :\r\n";
 // for (int i = 0; i < 49; i++) {
 //   sprintf(dbg_buffer2, "%02X", buffer[i]);
 //   strcat(dbg_buffer, dbg_buffer2);
 // }
 // f << dbg_buffer;
 // *dbg_buffer = 0;
  
  spi.readWrite(buffer, buffer, 1 + 12*4);

 // f << "\r\nRead from FPGA : \r\n";
 // for (int i = 0; i < 49; i++) {
 //   sprintf(dbg_buffer2, "%02X", buffer[i]);
 //   strcat(dbg_buffer, dbg_buffer2);
 // }
 // f << dbg_buffer << "\r\n";
 // *dbg_buffer = 0;
}