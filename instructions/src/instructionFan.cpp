
#include <instructionFan.hpp>
#include <temperatureController.hpp>
#include <host.hpp>
#include <printer.hpp>
#include <cmath>


/* Mapping functions */
instructionDefinition InstructionFan::getInstructionDefinition() {
  instructionDefinition def;
  def.constructor = InstructionFan::mapHook;
  def.gcodeTokens = getGcodeTokens();
  return def;
}

std::vector<std::string> InstructionFan::getGcodeTokens() {
  std::vector<std::string> list;

  /* List of all gcode tokens accepted */
  list.push_back("M106");
  list.push_back("M107");

  return list;
}

Instruction* InstructionFan::mapHook(std::string gcode) {
  return new InstructionFan(gcode);
}

/* TODO: Handle multiple fans and stuff */
InstructionFan::InstructionFan(std::string gcode) {

  auto gcodeData = instructionUtils::parseArguments(gcode);

  /* Check what gcode instruction was given */
  int gcodeNumber = std::round(gcodeData["M"]);
  switch(gcodeNumber) {
    case 106: 
      if (gcodeData.find("S") != gcodeData.end()) {
        val = gcodeData["S"] / 255.;
      } else {
        val = 1.;
      }
      break;

    case 107:
      val = 0.;
      break;
    default:
    break;
  }


}

InstructionFan::~InstructionFan() {

}

std::string InstructionFan::print() {
  std::string s;
  s += "Set fan speed to " + std::to_string(val * 100.) + "%";
  return s;
}

void InstructionFan::run() {

  /* TODO: sync to FPGA's FIFO */
  printer.setOutputPower(6, val);

}
