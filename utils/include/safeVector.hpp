#ifndef SAFEQVECTOR
#define SAFEQVECTOR

#include <vector>
#include <mutex>
#include <thread>

/* Thread-safe wrapper for std::vector
   Only one thread should consume data */
template <class VectorType>
class SafeVector {
  public:

  auto size() {
    m.lock();
    auto ret = v.size();
    m.unlock();
    return ret;
  }

  VectorType& at(int i) {
    m.lock();
    VectorType& ret = v.at(i);
    m.unlock();
    return ret;
  }

  void erase(int i) {
    m.lock();
    v.erase(v.begin() + i);
    m.unlock();
  }

  auto push_back(VectorType &in) {
    m.lock();
    v.push_back(in);
    m.unlock();
  }

  private:
  std::mutex m;
  std::vector<VectorType> v;
};


#endif /* SAFEQVECTOR */
