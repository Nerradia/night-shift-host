#include "spi.hpp"
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

std::mutex Spi::m;

Spi::Spi() {

}

void Spi::init(const char* dev, int spi_mode, uint32_t clk_speed) {
  int ret = 0;
  
  /* Open device */
	fd = open(dev, O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "Can't open %s\r\n", dev);
    return;
  }

  /* Set mode */
  int mode = 0;
  switch (mode) {
    case 0:
      mode = SPI_MODE_0;
      break;
    
    case 1:
      mode = SPI_MODE_1;
      break;

    case 2:
      mode = SPI_MODE_2;
      break;

    case 3:
      mode = SPI_MODE_3;
      break;

    default:
		  fprintf(stderr, "Unexpected SPI mode requested : %d\r\n", spi_mode);
      return;
  }

	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1) {
		fprintf(stderr, "Error setting SPI mode\r\n");
    return;
  }

  /* Set bits per word to 8 */
  int bits_per_word = 8;
  ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits_per_word);
	if (ret == -1) {
		fprintf(stderr, "Error setting SPI bit length\r\n");
    return;
  }

  /* Set speed */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &clk_speed);
	if (ret == -1) {
		fprintf(stderr, "Error setting SPI speed\r\n");
    return;
  }
  
}

int Spi::readWrite(char *data_in, char *data_out, uint32_t size) {
  int ret = 0;

	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)data_in,
		.rx_buf = (unsigned long)data_out,
		.len = size,
		.speed_hz = 0,
		.delay_usecs = 0,
		.bits_per_word = 0,
	};

  m.lock();

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);

  m.unlock();
  return ret;
}

