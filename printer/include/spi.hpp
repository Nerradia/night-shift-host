#ifndef SPI_HPP
#define SPI_HPP

#include <stdint.h>
#include <mutex>

class Spi {
  public :
  Spi();
  void init(const char* dev, int spi_mode, uint32_t clk_speed);
  int readWrite(char *data_in, char *data_out, uint32_t size);

  private :
  int fd;
  static std::mutex m;
  

};

#endif
