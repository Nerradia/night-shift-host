#ifndef MOVE
#define MOVE

#include <config.hpp>
#include <axesVector.hpp>

/* A movement, straight or curvy, in floating point format */
class Move {
  public:
  Move();

  /* Mandatory data */
  /* Move start coordinates */
  AxesVector origin;
  /* Move end coordinates */
  AxesVector target;
  /* Move speed before interpolation and acceleration planning */
  double targetSpeed;
  
  /* When curvy, those are set */
  /* Is it a bezier move */
  bool bezier;
  /* Second point of the Bezier curve */
  AxesVector bezier_p1;
  /* Third point of the Bezier curve */
  AxesVector bezier_p2;

  /* Acceleration-planning data */
  /* Combined speed at origin */
  double startSpeed;
  /* Combined speed at target */
  double endSpeed;

  /* Tangent normalized vector of the move's start */
  AxesVector getStartTangent();

  /* Tangent normalized vector of the move's end */
  AxesVector getEndTangent();
  
  /* Normalized vector of the move before Bezier */
  AxesVector getNormalized();

  /* Speed of axes before acceleration planning */
  double getTargetSpeed(int axis);
  AxesVector getTargetSpeed();

  /* Length of the move */
  double length();
  
  /* Target time of the move before acceleration planning */
  double getTargetTime();

  /* Target move relative to origin position */
  AxesVector relative();

  /* Target move relative to origin position */
  double relative(int i);

  /* Returns a position on the line or Bezier curve. p is between 0 and 1 */
  AxesVector getSample(double p);

  /* Debug print */
  std::string print();
  
};

#endif /* MOVE */
