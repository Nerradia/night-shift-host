#ifndef TELNET_INPUT_
#define TELNET_INPUT_

#include <iostream>
#include <thread.hpp>

class TelnetInput : public thread {
  public:
  TelnetInput();
  ~TelnetInput();

  void process();
  std::string threadName();

  private:

};

#endif /* TELNET_INPUT_ */
