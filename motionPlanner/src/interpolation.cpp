#include <interpolation.hpp>
#include <move.hpp>
#include <axesVector.hpp>
#include <cmath>


static double angleBetweenMove(Move a, Move b);
static double dotProd(AxesVector a, AxesVector b);


Interpolation::Interpolation(SafeQueue<Move>* inputQueue, SafeVector<Move>* accelQueue) {
  this->inputQueue = inputQueue;
  this->accelQueue = accelQueue;
  flushRequest = false;
}

std::string Interpolation::threadName() {
  return std::string("Interpolation");
}

Interpolation::~Interpolation() {
  
}

void Interpolation::flush() {
  flushRequest = true;
}

void Interpolation::process() {
  while(!stop_request) {

    /* Vector of moves to interpolate */
    std::vector<Move> group;
    
    /* Wait for at least one segment */
    auto inputSize = inputQueue->size();
    while (inputSize < 1) {
      inputSize = inputQueue->size();
      status = E_STATUS_IDLE;
      flushRequest = false;
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    } 
    
    /* handle flush requests */
    if (flushRequest) {
      inputQueue->flush();
      continue;
    }

    /* If output queue is full, wait */
    while (accelQueue->size() > Config::motionPlannerAccelQueueSize) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      status = E_STATUS_IDLE;
    }

    status = E_STATUS_RUNNING;

    trace("New interpolation group started, input queue size : " + std::to_string(inputSize));

    /* Should we continue to interpolate ? If not, the interpolation vector will stop here */
    bool interpolate = true;
    
    /* The new group begins with the first segment from the input queue,
       this will be used for interpolation criterias later */
    Move firstMoveOfGroup = inputQueue->get();

    /* Fill the vector */
    do {

      /* Pop the first segment from the input queue */
      Move currentMove = inputQueue->pop();
      trace("Current " + currentMove.print());

      /* Only one segment in queue => stop interpolating */
      if (inputSize == 1) {
        trace("Input is empty, ending group");
        interpolate = false;
      } 
      /* More than one segment => Try to match some criterias to see if we
         should stop */
      else {
        /* Peek at next segment */
        Move nextMove = inputQueue->get();

        /*    Interpolation criterias    */

        /* Speed change */
        if (currentMove.targetSpeed != nextMove.targetSpeed) {
          trace("Speed change, ending group");
          interpolate = false;
        }

        /* Current segment is long */
        else if (currentMove.length() > Config::interpMaxLength) {
          trace("current segment is long, ending group");
          interpolate = false;
        }

        /* Next segment is long */
        else if (nextMove.length() > Config::interpMaxLength) {
          trace("next segment is long, ending group");
          interpolate = false;
        }

        /* Length ratio between current and next segment are very different */
        else if (nextMove.length() / currentMove.length() > Config::interpMaxRatio) {
          trace("segment ratio is big, ending group");
          interpolate = false;
        }
        else if (currentMove.length() / nextMove.length() > Config::interpMaxRatio) {
          trace("segment ratio is big, ending group");
          interpolate = false;
        }

        /* Angle is big */
        else if (angleBetweenMove(currentMove, nextMove) > Config::interpMaxAngle) {
          trace("Angle is big, ending group");
          interpolate = false;
        }

        /* maximum accumulated distance ? */
        /* Angle from group start ? */
        //trace("remove before flight");
        //interpolate = false;

      }

      /* Add the current segment to the group */
      group.push_back(currentMove);

    } while (interpolate && !flushRequest);

    /* Only one segment => not something to apply Beziers to */
    if (group.size() == 1) {
      /* Get the move and throw it in the acceleration planner queue */
      auto m = group.at(0);
      accelQueue->push_back(m);
      trace("Queued segment " + m.print() + " with bezier = false");
    }
    /* Calculate Bezier's points in the group */
    else {
      for (int i = 0; i < group.size(); i++) {
        /* Beziers are defined by a set of 4 points : 
        p0 : starting point of the move;
        p1 : 1st control point of the move, set according to the previous move;
        p2 : 2nd control point, set according to the next move;
        p3 : ending point of the move */

        /* Calculate p1 */
        if (i == 0) {
          /* First move of group, no interpolation (TODO) */
          /* p1 is placed on the current segment, at 33% of its length */
          group[i].bezier_p1 = group[i].origin + group[i].relative() * 0.33;
        }
        else {
          /* p1 is placed on the ~average tangent~ between previous and current segment */
          //AxesVector transitionSpeed = group[i-1].getTargetSpeed() / 2 + group[i].getTargetSpeed() / 2;
          //group[i].bezier_p1 = group[i].origin + transitionSpeed * 0.33 * group[i].getTargetTime();
          AxesVector tangent = (group[i-1].getNormalized() / 2 + group[i].getNormalized() / 2);
          group[i].bezier_p1 = group[i].origin + tangent * group[i].length() * 0.33;
        }

        /* Calculate p2 */
        if (i == group.size() - 1) {
          /* Last move of group, no interpolation (TODO) */
          /* p2 is placed on the current segment, at 66% of its length */
          group[i].bezier_p2 = group[i].origin + group[i].relative() * 0.66;
        }
        else {
          /* p2 is placed on the ~average tangent~ between current and next segment */
          //AxesVector transitionSpeed = group[i].getTargetSpeed() / 2 + group[i+1].getTargetSpeed() / 2;
          //group[i].bezier_p2 = group[i].target - transitionSpeed * 0.33 * group[i].getTargetTime();
          AxesVector tangent = (group[i].getNormalized() / 2 + group[i+1].getNormalized() / 2);
          group[i].bezier_p2 = group[i].target - tangent * group[i].length() * 0.33;
        }

        /* Set the bezier flag for next processing */
        group[i].bezier = true;

        /* Send to the next thread */
        accelQueue->push_back(group[i]);

        trace("Queued segment " + group[i].print() + " with p1 = " + group[i].bezier_p1.print() + " and p2 = " + group[i].bezier_p2.print());
      }
    }
  }
}


/* https://www.mathsisfun.com/algebra/vectors-dot-product.html */
static double angleBetweenMove(Move a, Move b) {
  double cosTheta = dotProd(a.relative(), b.relative()) / (a.length() * b.length());
  if (cosTheta > 1) cosTheta = 1;
  return acos(cosTheta);
}

static double dotProd(AxesVector a, AxesVector b) {
  auto vect_prod = a * b;
  return vect_prod.sum();
}

