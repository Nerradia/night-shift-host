#ifndef ACCELERATIONPLANNER
#define ACCELERATIONPLANNER

#include <thread.hpp>
#include <move.hpp>
#include <safeVector.hpp>
#include <safeQueue.hpp>

class AccelerationPlanner : public thread {
  public:
    std::string threadName();
    AccelerationPlanner(SafeVector<Move> *accelQueue, SafeQueue<Move> *outputQueue);
    ~AccelerationPlanner();
    void process();
    void flush();

  private:
  bool flushRequest;
  /* How many paths are affected by the "assumed end" of the travel, those 
     shouldn't be sent except if we are starving */
  double maxDecelerationTime;

  /* Queue shared between the interpolation thread and the acceleration planner
     thread. Vector instead of a queue because acceleration thread needs to see
     the whole data */
  SafeVector<Move> *accelQueue;
  /* Queue between the acceleration planner and the motion output thread */
  SafeQueue<Move> *outputQueue;

  /* Tells how many samples are needed for the sum to exceed the input
     it starts at the *back* of the accelQueue vector */
  unsigned int howManySamplesForTime(double time);

  /* Lowers the start and stop speed of the 2nd move based on :
     - the direction of both moves
     - the end speed of the 1st move */
  void plannifyAcceleration(Move &a, Move &b);
  
  /* Lowers the start and end speed of the 2nd move based on :
     - the direction of both moves
     - the start speed of the 1st move */
  void plannifyDeceleration(Move &a, Move &b);
};

#endif /* ACCELERATIONPLANNER */
